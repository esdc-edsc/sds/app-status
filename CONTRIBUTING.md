# Contributing

## How to Contribute

Every employee in Government of Canada is welcome to suggest changes to this application, although those outside Employment and Social Development Canada may have some difficulties matching the [Development Environment Setup](#development-environment-setup).

When contributing, post comments and discuss changes you wish to make via Issues.

Feel free to propose changes by creating Merge Requests. If you don't have write access, editing a file will create a Fork of this project for you to save your proposed changes to. Submitting a change to a file will write it to a new Branch in your Fork, so you can send a Merge Request.

If this is your first time contributing on GitLab, don't worry! Let us know if you have any questions.

## Conduct Expectations

Please be aware of the [Code Of Conduct](CODE_OF_CONDUCT.md) expectations on behavior when contributing to the product.

## Security

**Mark security issues as confidential!**

## Making Changes

To start making changes you will need to Fork the project if you are not a member of it.
You will also need to set up your [development environment](_docs/EnvironmentSetup.md) and add a GitLab-Runner that meets the [Pipeline Configurations](_docs/PipelineConfiguration.md).

### Submitting for Review

Please follow the best practices defined in the ESDC recommendation for [Managing Merge Requests, Using Code Reivew](https://esdc-devcop.github.io/recommendations/merging-review.html) as the _author_.

### Reviewing a Merge Requests

Please follow the best practices defined in the ESDC recommendation for [Managing Merge Requests, Using Code Reivew](https://esdc-devcop.github.io/recommendations/merging-review.html) as the _reviewer_.
