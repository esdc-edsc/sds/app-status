﻿using AutoMapper;
using GoC.AppStatus.DBAccess.DBEntities;
using GoC.AppStatus.WebClient.Models;

namespace GoC.AppStatus.WebClient
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<UserModel, User>();
            CreateMap<UserSignInModel, User>();
            CreateMap<ServiceModel, Service>();
            CreateMap<Service, ServiceModel>();
            CreateMap<Service, ServiceViewModel>();
            CreateMap<ServiceView, ServiceViewModel>();
            CreateMap<EnvironmentModel, Environment>();
            CreateMap<Environment, EnvironmentModel>();
        }
    }
}
