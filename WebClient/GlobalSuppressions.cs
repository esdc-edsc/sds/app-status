﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pending>", Scope = "member", Target = "~M:GoC.AppStatus.WebClient.Startup.Configure(Microsoft.AspNetCore.Builder.IApplicationBuilder,Microsoft.AspNetCore.Hosting.IHostingEnvironment)")]
[assembly: SuppressMessage("Design", "CA1051:Do not declare visible instance fields", Justification = "<Pending>", Scope = "member", Target = "~F:GoC.AppStatus.WebClient.Controllers.BaseController.Logger")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "Already validated.", Scope = "member", Target = "~M:GoC.AppStatus.WebClient.Controllers.BaseController.SetAuthentication(Microsoft.AspNetCore.Mvc.Filters.ActionExecutingContext)")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "Already validated.", Scope = "member", Target = "~M:GoC.AppStatus.WebClient.Controllers.BaseController.SetWetTemplateAppDefaults(Microsoft.AspNetCore.Mvc.Filters.ActionExecutingContext)")]
[assembly: SuppressMessage("Design", "CA1065:Do not raise exceptions in unexpected locations", Justification = "Exception is needed", Scope = "member", Target = "~P:GoC.AppStatus.WebClient.Controllers.BaseController.DBAccess")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pending>", Scope = "member", Target = "~M:GoC.AppStatus.WebClient.Configs.ConfigurationExtention.BindObject``1(Microsoft.Extensions.Configuration.IConfiguration,System.String)~``0")]
[assembly: SuppressMessage("Design", "CA1051:Do not declare visible instance fields", Justification = "<Pending>", Scope = "member", Target = "~F:GoC.AppStatus.WebClient.Controllers.BaseController.Mapper")]
[assembly: SuppressMessage("Design", "CA1051:Do not declare visible instance fields", Justification = "<Pending>", Scope = "member", Target = "~F:GoC.AppStatus.WebClient.Controllers.BaseController.Configuration")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pending>", Scope = "member", Target = "~M:GoC.AppStatus.WebClient.Controllers.AccountController.Register(GoC.AppStatus.WebClient.Models.UserModel)~Microsoft.AspNetCore.Mvc.IActionResult")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pending>", Scope = "member", Target = "~M:GoC.AppStatus.WebClient.Controllers.AccountController.Login(GoC.AppStatus.WebClient.Models.UserSignInModel)~Microsoft.AspNetCore.Mvc.IActionResult")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pending>", Scope = "member", Target = "~M:GoC.AppStatus.WebClient.Controllers.ServicesController.Register(GoC.AppStatus.WebClient.Models.ServiceModel)~Microsoft.AspNetCore.Mvc.IActionResult")]
[assembly: SuppressMessage("Design", "CA1054:Uri parameters should not be strings", Justification = "<Pending>", Scope = "member", Target = "~M:GoC.AppStatus.WebClient.Controllers.ServicesController.Subscribe(System.String)")]
[assembly: SuppressMessage("Design", "CA1055:Uri return values should not be strings", Justification = "<Pending>", Scope = "member", Target = "~M:GoC.AppStatus.WebClient.Controllers.ServicesController.GetCanonicalUrl(System.String)~System.String")]
[assembly: SuppressMessage("Design", "CA1054:Uri parameters should not be strings", Justification = "<Pending>", Scope = "member", Target = "~M:GoC.AppStatus.WebClient.Controllers.ServicesController.GetCanonicalUrl(System.String)~System.String")]
