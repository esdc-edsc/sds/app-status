﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GoC.AppStatus.WebClient.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class UsersModelResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal UsersModelResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("GoC.AppStatus.WebClient.Resources.UsersModelResources", typeof(UsersModelResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to AppStatus Home.
        /// </summary>
        public static string AppStatus {
            get {
                return ResourceManager.GetString("AppStatus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This field must match the Password field..
        /// </summary>
        public static string ConfirmPassword {
            get {
                return ResourceManager.GetString("ConfirmPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit Service.
        /// </summary>
        public static string EditService {
            get {
                return ResourceManager.GetString("EditService", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit User.
        /// </summary>
        public static string EditUser {
            get {
                return ResourceManager.GetString("EditUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email address not valid..
        /// </summary>
        public static string EmailNotValid {
            get {
                return ResourceManager.GetString("EmailNotValid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid characters found..
        /// </summary>
        public static string InvalidCharacters {
            get {
                return ResourceManager.GetString("InvalidCharacters", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The domain is not valid..
        /// </summary>
        public static string InvalidDomain {
            get {
                return ResourceManager.GetString("InvalidDomain", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please enter at least 10 characters..
        /// </summary>
        public static string PasswordMinLen {
            get {
                return ResourceManager.GetString("PasswordMinLen", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The password must contain at least one lowercase letter, one uppercase letter, one special character and one number..
        /// </summary>
        public static string PasswordStrength {
            get {
                return ResourceManager.GetString("PasswordStrength", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Register Service.
        /// </summary>
        public static string RegisterService {
            get {
                return ResourceManager.GetString("RegisterService", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Register User.
        /// </summary>
        public static string RegisterUser {
            get {
                return ResourceManager.GetString("RegisterUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This field is required..
        /// </summary>
        public static string RequiredField {
            get {
                return ResourceManager.GetString("RequiredField", resourceCulture);
            }
        }
    }
}
