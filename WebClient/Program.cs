using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.HttpSys;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NLog.Web;
using System;

namespace GoC.AppStatus.WebClient
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            // NLog: setup the logger first to catch all errors
            var logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
            try
            {
                logger.Info("[Program > Main] AppStatus started.");
                //CreateWebHostBuilder(args).UseKestrel().Build().Run(); // core 2.1
                CreateHostBuilder(args).Build().Run(); // core 3.1
            }
            catch (Exception ex)
            {
                //NLog: catch setup errors
                logger.Error(ex, "Stopped program because of exception");
                throw;
            }
            finally
            {
                // Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
                NLog.LogManager.Shutdown();
            }
        }

        /*public static IWebHostBuilder CreateWebHostBuilder(string[] args) => // core 2.1 (keep for now; to delete later)
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseHttpSys(options =>
                {
                    options.Authentication.Schemes =
                        AuthenticationSchemes.NTLM |
                        AuthenticationSchemes.Negotiate;
                    options.Authentication.AllowAnonymous = true;
                })
                .ConfigureLogging(logging =>
                {
                    logging.ClearProviders();
                    // logging.SetMinimumLevel(LogLevel.Trace); without effect on Azure AI
                })
                .UseNLog(); // NLog: setup NLog for Dependency injection */


        public static IHostBuilder CreateHostBuilder(string[] args) => // core 3.1
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseKestrel((builderContext, kestrelOptions) =>
                    {
                        //kestrelOptions.Configure(builderContext.Configuration.GetSection("Kestrel")); no options inside appsettings.json for now...
                    });

                    /*webBuilder.UseHttpSys(options =>
                     {
                         options.Authentication.Schemes =
                             AuthenticationSchemes.NTLM |
                             AuthenticationSchemes.Negotiate;
                         options.Authentication.AllowAnonymous = true;
                     });*/ // => "HTTP Error 502.5 - ANCM Out-Of-Process Startup Failure"; to investigate later if needed.

                    webBuilder.UseStartup<Startup>();
                })

                .ConfigureLogging(logging =>
                {
                    logging.ClearProviders();
                    // logging.SetMinimumLevel(LogLevel.Trace); without effect on Azure AI
                })
                .UseNLog(); // NLog: setup NLog for Dependency injection

    }
}
