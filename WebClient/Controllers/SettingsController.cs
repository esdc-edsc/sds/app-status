﻿using AutoMapper;
using GoC.AppStatus.DBAccess.DataAccess;
using GoC.AppStatus.WebClient.Models;
using GoC.WebTemplate.Components.Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System.Net;

namespace GoC.AppStatus.WebClient.Controllers
{
    [Authorize]
    public class SettingsController : BaseController
    {
        private readonly IStringLocalizer<SettingsController> _localizer;
        private readonly IUserQueries _userQueries;

        public SettingsController(ModelAccessor modelAccessor,
                               IMapper mapper,
                               IConfiguration configuration,
                               ILogger<SettingsController> logger,
                               IUserQueries userQueries,
                               IStringLocalizer<BaseController> baseLocalizer,
                               IStringLocalizer<SettingsController> localizer) :
            base(modelAccessor, mapper, configuration, logger, baseLocalizer)
        {
            _localizer = localizer;
            _userQueries = userQueries;
        }
        
        public IActionResult Settings()
        {
            return View();
        }

    }
}
