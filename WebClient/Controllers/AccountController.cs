using AutoMapper;
using GoC.AppStatus.DBAccess.DataAccess;
using GoC.AppStatus.WebClient.Models;
using GoC.WebTemplate.Components.Core.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System.Security.Claims;
using System.Collections.Generic;
using GoC.AppStatus.DBAccess.DBEntities;
using GoC.AppStatus.WebClient.Configs;

namespace GoC.AppStatus.WebClient.Controllers
{
    public class AccountController : BaseController
    {
        private readonly IStringLocalizer<AccountController> _localizer;
        private readonly IHttpContextAccessor _httpContext;
        private readonly IUserQueries _userQueries;
        private IUserQueries UserQueries => (IUserQueries)ConnectDatabaseSQL(_userQueries);

        public AccountController(ModelAccessor modelAccessor,
                               IMapper mapper,
                               IConfiguration configuration,
                               ILogger<AccountController> logger,
                               IUserQueries userQueries,
                               IStringLocalizer<BaseController> baseLocalizer, 
                               IStringLocalizer<AccountController> localizer,
                               IHttpContextAccessor httpContext) :
            base(modelAccessor, mapper, configuration, logger, baseLocalizer)
        { 
            _localizer = localizer;
            _httpContext = httpContext;
            _userQueries = userQueries;
        }        

        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Register(UserModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.CheckEmailDuplicate(UserQueries))
                {
                    ModelState.AddModelError("Email", _localizer["The Email address entered is already in use."]);
                }

                if (ModelState.IsValid)
                {
                    var user = model.Add(UserQueries, Mapper);
                    LoginUser(user);
                    return RedirectToAction("Index", "Home");
                }
            }

            return View(model);
        }

        public IActionResult Login()
        {
            SetLoginWindowToOpen();
            return View();
        }

        [HttpPost]
        public IActionResult Login(UserSignInModel model)
        {
            if (ModelState.IsValid)
            {
                var user = model.Authenticate(UserQueries, Mapper);
                if (user == null)
                {
                    ModelState.AddModelError(nameof(model.SignInEmail), _localizer["The Email or Password is invalid."]);
                }
                else
                {
                    LoginUser(user);
                }
            }

            if (ModelState.IsValid)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                SetLoginWindowToOpen();
                return View();
            }
        }

        /// <summary>
        /// Removes User Claims
        /// </summary>
        /// <returns></returns>
        public IActionResult Logout()
        {
            _httpContext.HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Adds User Claims to HttpContext
        /// </summary>
        /// <param name="user"></param>
        private void LoginUser(User user)
        {
            if (user != null)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Email, user.Email),
                    new Claim(ClaimTypes.Name, user.ActualName),
                    new Claim(CustomClaimTypes.Identifier, user.Id.ToString())
                };

                var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                var authProperties = new AuthenticationProperties();

                _httpContext.HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity), authProperties);
            }
        }

        private void SetLoginWindowToOpen()
        {
            //trigger the lightbox modal to force open showing errors
            WebTemplateModel.HTMLBodyElements.Add("<script>$(document).trigger(\"open.wb-lbx\", [[{src:\"#login\",type:\"inline\"}]]);</script>");
        }
    }
}