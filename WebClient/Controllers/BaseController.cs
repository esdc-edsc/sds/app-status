﻿using AutoMapper;
using GoC.AppStatus.DBAccess.DataAccess;
using GoC.AppStatus.WebClient.Configs;
using GoC.WebTemplate.Components.Core.Services;
using GoC.WebTemplate.Components.Entities;
using GoC.WebTemplate.CoreMVC.Controllers;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace GoC.AppStatus.WebClient.Controllers
{
    public class BaseController : WebTemplateBaseController
    {
        protected readonly IMapper Mapper;
        protected readonly IConfiguration Configuration;
        protected readonly ILogger Logger;

        private readonly IStringLocalizer<BaseController> _localizer;

        public BaseController(ModelAccessor modelAccessor,
                              IMapper mapper,
                              IConfiguration configuration,
                              ILogger logger,
                              IStringLocalizer<BaseController> localizer) 
            : base(modelAccessor)
        {
            Mapper = mapper;
            Configuration = configuration;
            Logger = logger;
            _localizer = localizer;
        }

        protected ISQLAccess ConnectDatabaseSQL(ISQLAccess dbAccess)
        {
            if (dbAccess == null) throw new ArgumentNullException(nameof(dbAccess));
            if (!dbAccess.DatabaseHasConnection()) dbAccess.ConnectDatabase(Configuration.GetConnectionString("DefaultConnection"));
            return dbAccess;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context)); //force failure if the context is null as the app can't run without it
            base.OnActionExecuting(context);

            SetAuthentication(context);
            SetWetTemplateAppDefaults(context);
        }

        public void SetAuthentication(ActionExecutingContext context)
        {
            var authConfig = Configuration.BindObject<AuthConfig>("AuthConfig");

            if (authConfig.Method == AuthMethod.InternalAD)
            {
                throw new NotImplementedException();
                //TODO: Add AD user info to User Claims  
            }
            else if (authConfig.Method == AuthMethod.SelfManaged)
            {
                if (User.Identity.IsAuthenticated)
                {
                    WebTemplateModel.ShowSignInLink = false;
                    WebTemplateModel.ShowSignOutLink = true;
                    WebTemplateModel.Settings.SignOutLinkUrl = "/Account/Logout";
                }
                else
                {
                    WebTemplateModel.ShowSignInLink = true;
                    WebTemplateModel.ShowSignOutLink = false;
                    WebTemplateModel.Settings.SignInLinkUrl = "#login";
                }
            }
        }

        public void SetWetTemplateAppDefaults(ActionExecutingContext context)
        {
            //set enviornment notice
            var url = context.HttpContext.Request.GetDisplayUrl();
            var appendTitleText = string.Empty;
            if (url.Contains("localhost", StringComparison.OrdinalIgnoreCase))
            {
                appendTitleText = " [LocalHost]";
            }
            else if (url.Contains("staging", StringComparison.OrdinalIgnoreCase))
            {
                appendTitleText = " [Staging]";
            }

            WebTemplateModel.ApplicationTitle.Text = _localizer["App Status"] + appendTitleText + " (Alpha)";
            WebTemplateModel.ApplicationTitle.Href = Url.Action("Index", "Home");
            WebTemplateModel.MenuLinks = GetMenu();

            //set breadcrumbs and page title based on mvc action/controller
            WebTemplateModel.HeaderTitle = _localizer["App Status"];
            var actionName = context.RouteData.Values["action"].ToString();
            if (!actionName.Equals("Index", StringComparison.CurrentCultureIgnoreCase))
            {
                WebTemplateModel.Breadcrumbs.Add(new Breadcrumb { Href = Url.Action("Index", "Home"), Title = Resources.UsersModelResources.AppStatus });
                var actionTranslation =
                    RouteData.Values["Controller"].Equals("Environments") ?
                    Resources.Breadcrumbs.EnvironmentsResources.ResourceManager.GetString(actionName, CultureInfo.CurrentUICulture) :
                    RouteData.Values["Controller"].Equals("Services") ?
                    Resources.Breadcrumbs.ServicesResources.ResourceManager.GetString(actionName, CultureInfo.CurrentUICulture) :
                    RouteData.Values["Controller"].Equals("Settings") ?
                    Resources.Breadcrumbs.SettingsResources.ResourceManager.GetString(actionName, CultureInfo.CurrentUICulture) :
                    Resources.Breadcrumbs.AccountResources.ResourceManager.GetString(actionName, CultureInfo.CurrentUICulture);
                WebTemplateModel.Breadcrumbs.Add(new Breadcrumb
                {
                    Title = actionTranslation
                });
                WebTemplateModel.HeaderTitle = WebTemplateModel.HeaderTitle.Insert(0, actionTranslation + " | ");
            }
        }

        public List<MenuLink> GetMenu()
        {
            if (!User.Identity.IsAuthenticated) return null;

            List<MenuLink> menu = new List<MenuLink>
            {
                new MenuLink { Href = Url.Action("Index", "Home"), Text = _localizer["Home"] },
                new MenuLink { Href = Url.Action("Register", "Services"), Text = _localizer["Register Service"] },
                new MenuLink { Href = Url.Action("Settings", "Settings"), Text = _localizer["Settings"] }
            };

            return menu;
        }
    }
}
