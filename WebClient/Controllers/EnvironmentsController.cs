﻿using AutoMapper;
using GoC.AppStatus.DBAccess.DataAccess;
using GoC.AppStatus.DBAccess;
using GoC.AppStatus.WebClient.Configs;
using GoC.AppStatus.WebClient.Models;
using GoC.WebTemplate.Components.Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net;
using System.Security.Claims;

namespace GoC.AppStatus.WebClient.Controllers
{
    [Authorize]
    public class EnvironmentsController : BaseController
    {
        private readonly IStringLocalizer<EnvironmentsController> _localizer;
        private readonly IEnvironmentQueries _environmentQueries;
        private IEnvironmentQueries EnvironmentQueries => (IEnvironmentQueries)ConnectDatabaseSQL(_environmentQueries);
        private readonly IUserQueries _userQueries;
        public const string ErrorMsgTitleExists = "The title entered has already been used.";
        public const string ErrorMsgCannotEdit = "You cannot edit this environment, because you did not create it!";
        public const string ErrorMsgCannotRemove = "You cannot remove this environment, because you did not create it!";
        public const string ErrorMsgUsedByAtLeastOneService = "You cannot remove this environment, because it's used by at least one service!";
        public const string ErrorMsgRemovingError = "An error occurred during environment removing!";

        public EnvironmentsController(ModelAccessor modelAccessor,
                               IMapper mapper,
                               IConfiguration configuration,
                               ILogger<SettingsController> logger,
                               IEnvironmentQueries environmentQueries,
                               IUserQueries userQueries,
                               IStringLocalizer<BaseController> baseLocalizer,
                               IStringLocalizer<EnvironmentsController> localizer) :
            base(modelAccessor, mapper, configuration, logger, baseLocalizer)
        {
            _localizer = localizer;
            _environmentQueries = environmentQueries;
            _userQueries = userQueries;
        }

        public IActionResult Create()
        {
            // the View needs the list of environments created by the user:
            var userId = Convert.ToUInt32(User.FindFirstValue(CustomClaimTypes.Identifier), 10);
            IEnumerable<EnvironmentModel> environmentModels = EnvironmentModel.GetAllEnvironments(EnvironmentQueries, Mapper, userId);
            ViewBag.Environments = environmentModels;

            return View();
        }

        [HttpPost]
        public IActionResult Create(EnvironmentModel model)
        {
            if (model == null) throw new System.Web.Http.HttpResponseException(HttpStatusCode.BadRequest);

            var userId = Convert.ToUInt32(User.FindFirstValue(CustomClaimTypes.Identifier), 10);

            if (ModelState.IsValid)
            {
                if (model.CheckForTitleDuplicate(EnvironmentQueries, userId))
                {
                    ModelState.AddModelError("EnvTitle", _localizer[ErrorMsgTitleExists]);
                }
            }

            if (ModelState.IsValid)
            {
                model.Add(EnvironmentQueries, Mapper, userId);
                return RedirectToAction("Create", "Environments");
            }
            else
            {
                IEnumerable<EnvironmentModel> environmentModels = EnvironmentModel.GetAllEnvironments(EnvironmentQueries, Mapper, userId);
                ViewBag.Environments = environmentModels;
                return View(model);
            }
        }

        public IActionResult Edit(string id)
        {
            if (id == null) throw new System.Web.Http.HttpResponseException(HttpStatusCode.BadRequest);

            var userId = Convert.ToUInt32(User.FindFirstValue(CustomClaimTypes.Identifier), 10);
            DBAccess.DBEntities.Environment environment = _environmentQueries.GetEnvironmentById(Convert.ToUInt32(id, 10));
            EnvironmentModel.CheckIfUserAuthorizeToEdit(environment, userId, ErrorMsgCannotEdit);

            var model = Mapper.Map<EnvironmentModel>(environment);
            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(string id, EnvironmentModel model)
        {
            if (id == null || model == null) throw new System.Web.Http.HttpResponseException(HttpStatusCode.BadRequest);

            var userId = Convert.ToUInt32(User.FindFirstValue(CustomClaimTypes.Identifier), 10);
            DBAccess.DBEntities.Environment environment = _environmentQueries.GetEnvironmentById(Convert.ToUInt32(id, 10));
            EnvironmentModel.CheckIfUserAuthorizeToEdit(environment, userId, ErrorMsgCannotEdit);

            if (ModelState.IsValid)
            {
                var currentTitle = environment.Title;
                if (currentTitle != model.Title && EnvironmentQueries.IsEnvTitlelUsed(model.Title, userId))
                {
                    ModelState.AddModelError("Title", _localizer[ErrorMsgTitleExists]);
                }
            }

            if (ModelState.IsValid)
            {
                model.Update(Convert.ToUInt32(id, 10), EnvironmentQueries, Mapper);
                return RedirectToAction("Create", "Environments");
            }
            else
            {
                return View(model);
            }
        }

        public string Remove(string id)
        {
            var userId = Convert.ToUInt32(User.FindFirstValue(CustomClaimTypes.Identifier), 10);
            DBAccess.DBEntities.Environment environment = _environmentQueries.GetEnvironmentById(Convert.ToUInt32(id, 10));
            EnvironmentModel.CheckIfUserAuthorizeToRemove(environment, userId, ErrorMsgCannotRemove);

            if (EnvironmentQueries.EnvironmentUsedByAtLeastOneService(Convert.ToUInt32(id, 10)))
            {
                return _localizer[ErrorMsgUsedByAtLeastOneService];
            }

            if (EnvironmentModel.Remove(Convert.ToUInt32(id, 10), EnvironmentQueries)) return "";
            else return _localizer[ErrorMsgRemovingError];
        }

    }
}
