﻿using AutoMapper;
using GoC.AppStatus.CheckService.Utilities;
using GoC.AppStatus.DBAccess.DataAccess;
using GoC.AppStatus.WebClient.Configs;
using GoC.AppStatus.WebClient.Models;
using GoC.WebTemplate.Components.Core.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace GoC.AppStatus.WebClient.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IServiceQueries _serviceQueries;
        private readonly ISubscriptionQueries _subscriptionQueries;
        private readonly IEnvironmentQueries _environmentQueries;
        private IServiceQueries ServiceQueries => (IServiceQueries)ConnectDatabaseSQL(_serviceQueries);
        private IEnvironmentQueries EnvironmentQueries => (IEnvironmentQueries)ConnectDatabaseSQL(_environmentQueries);

        public HomeController(ModelAccessor modelAccessor,
                              IMapper mapper,
                              IConfiguration configuration,
                              ILogger<HomeController> logger,
                              IServiceQueries serviceQueries,
                              ISubscriptionQueries subscriptionQueries,
                              IEnvironmentQueries environmentQueries,
                              IStringLocalizer<BaseController> baseLocalizer) :
            base(modelAccessor, mapper, configuration, logger, baseLocalizer)
        {
            _serviceQueries = serviceQueries;
            _subscriptionQueries = subscriptionQueries;
            _environmentQueries = environmentQueries;
        }

        public IActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                var userId = Convert.ToUInt32(User.FindFirstValue(CustomClaimTypes.Identifier), 10);
                if (userId > 0)
                {
                    var servicesModel = new DashboardViewModel
                    {
                        AllServices = Mapper.Map<IEnumerable<ServiceViewModel>>(ServiceQueries.GetAllServicesView(userId)),
                    };
                    return View("Dashboard", servicesModel);
                }
            }
            return View("Preview");
        }

        /// <summary>
        /// Send a test Email to the current User
        /// </summary>
        public void TestEmail()
        {
            var gcNotify = new GcNotifyService(Configuration);

            gcNotify.SendTestEmail(User.FindFirstValue(ClaimTypes.Email));
        }

        /// <summary>
        /// To display a custom Error message 
        /// </summary>
        /// <param name="error">ErrorModel containing a Title and an Error message.</param>
        /// <returns></returns>
        [Route("Error")]
        public IActionResult Error(ErrorModel error)
        {
            return View(error);
        }

        /// <summary>
        /// To display an Error page to handle HTTP status codes
        /// </summary>
        /// <param name="code">HTTP status code</param>
        /// <returns></returns>
        [Route("Error/{code:int}")]
        public IActionResult Error()
        {
            return View();
        }
    }
}