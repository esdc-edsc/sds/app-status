﻿using AutoMapper;
using GoC.AppStatus.CheckService.Utilities;
using GoC.AppStatus.DBAccess.DBEntities;
using GoC.AppStatus.DBAccess.DataAccess;
using GoC.AppStatus.WebClient.Configs;
using GoC.AppStatus.WebClient.Models;
using GoC.WebTemplate.Components.Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Security.Claims;
using System.Collections.Generic;

namespace GoC.AppStatus.WebClient.Controllers
{
    [Authorize]
    public class ServicesController : BaseController
    {
        private readonly IStringLocalizer<ServicesController> _localizer;
        private readonly IServiceQueries _serviceQueries;
        private readonly ISubscriptionQueries _subscriptionQueries;
        private readonly IUserQueries _userQueries;
        private readonly IEnvironmentQueries _environmentQueries;
        private IServiceQueries ServiceQueries => (IServiceQueries)ConnectDatabaseSQL(_serviceQueries);
        private IEnvironmentQueries EnvironmentQueries => (IEnvironmentQueries)ConnectDatabaseSQL(_environmentQueries);
        private ISubscriptionQueries SubscriptionQueries => (ISubscriptionQueries)ConnectDatabaseSQL(_subscriptionQueries);
        private readonly IServiceTester _serviceTester;
        public const string ErrorMsgCannotEdit = "You cannot edit this service, because you did not register it!";
        public const string ErrorMsgUrlExists = "The Url entered has already been added.";
        public const string ErrorMsgCannotRemove = "You cannot remove this service, because you did not register it!";
        public const string ErrorMsgUrlDoesntReturnValidResponse = "The Url entered doesn't return a valid response!";
        public const string ErrorMsgCannotSubscribe = "You cannot subscribe this service, because you did not register it!";
        public const string ErrorMsgCannotUnsubscribe = "You cannot unsubscribe this service, because you did not register it!";
        public const string ErrorMsgNameExists = "The name entered has already been added.";

        public ServicesController(ModelAccessor modelAccessor, 
                                  IMapper mapper, 
                                  IConfiguration configuration,
                                  ILogger<ServicesController> logger,
                                  IServiceQueries serviceQueries,
                                  ISubscriptionQueries subscriptionQueries,
                                  IUserQueries userQueries,
                                  IEnvironmentQueries environmentQueries,
                                  IStringLocalizer<BaseController> baseLocalizer,
                                  IStringLocalizer<ServicesController> localizer,
                                  IServiceTester serviceTester) :
            base(modelAccessor, mapper, configuration, logger, baseLocalizer)
        { 
            _localizer = localizer;
            _serviceQueries = serviceQueries;
            _subscriptionQueries = subscriptionQueries;
            _userQueries = userQueries;
            _environmentQueries = environmentQueries;
            _serviceTester = serviceTester;
        }

        public IActionResult Register()
        {
            var userId = Convert.ToUInt32(User.FindFirstValue(CustomClaimTypes.Identifier), 10);
            IEnumerable<EnvironmentModel> environmentModels = EnvironmentModel.GetAllEnvironments(EnvironmentQueries, Mapper, userId);
            ViewBag.Environments = environmentModels;

            return View();
        }

        [HttpPost]
        public IActionResult Register(ServiceModel model)
        {
            if (model == null) throw new System.Web.Http.HttpResponseException(HttpStatusCode.BadRequest);

            var userId = Convert.ToUInt32(User.FindFirstValue(CustomClaimTypes.Identifier), 10);

            if (ModelState.IsValid)
            {
                model.Url = GetCanonicalUrl(model.Url);

                if (model.CheckForUrlDuplicate(ServiceQueries, userId))
                {
                    ModelState.AddModelError("Url", _localizer[ErrorMsgUrlExists]);
                }
            }

            if (ModelState.IsValid)
            {
                if (model.CheckForNameDuplicate(ServiceQueries, userId))
                {
                    ModelState.AddModelError("Url", _localizer[ErrorMsgNameExists]);
                }
            }

            if (ModelState.IsValid)
            {
                if (!model.Url.Equals(Configuration.GetValue<string>("BypassURLValidation:Url"), StringComparison.OrdinalIgnoreCase))
                {
                    var currentStatusCode = _serviceTester.GetStatusCode(new Uri(model.Url));
                    if (!_serviceTester.IsStatusCodeValid(currentStatusCode))
                    {
                        ModelState.AddModelError("Url", _localizer[ErrorMsgUrlDoesntReturnValidResponse]);
                    }
                }
            }

            if (ModelState.IsValid)
            {
                model.Add(ServiceQueries, Mapper, userId);
                return RedirectToAction("Index","Home");
            }
            else
            {
                IEnumerable<EnvironmentModel> environmentModels = EnvironmentModel.GetAllEnvironments(EnvironmentQueries, Mapper, userId);
                ViewBag.Environments = environmentModels;
                return View(model);
            }
        }

        public bool Subscribe(string serviceId)
        {
            if (serviceId == null) throw new System.Web.Http.HttpResponseException(HttpStatusCode.BadRequest);

            uint id = Convert.ToUInt32(serviceId, 10);

            var userId = Convert.ToUInt32(User.FindFirstValue(CustomClaimTypes.Identifier), 10);
            Service service = _serviceQueries.GetServiceById(id);
            ServiceModel.CheckIfUserAuthorizeToSubscribe(service, userId, ErrorMsgCannotSubscribe);

            var subscriptionId = SubscriptionQueries.GetSubscriptionIdFromServiceId(id);
            if (subscriptionId == 0)
            {
                // subscription record doesn't exist; create one:
                var subscription = new Subscription
                {
                    ServiceId = id,
                    UserId = userId,
                    SubscribedDatetime = DateTime.Now
                };
                return SubscriptionQueries.AddSubscription(subscription);
            }

            // update subscription record:
            return SubscriptionQueries.Subscribe(subscriptionId);
        }

        public bool Unsubscribe(string serviceId)
        {
            if (serviceId == null) throw new System.Web.Http.HttpResponseException(HttpStatusCode.BadRequest);

            uint id = Convert.ToUInt32(serviceId, 10);
            var userId = Convert.ToUInt32(User.FindFirstValue(CustomClaimTypes.Identifier), 10);
            Service service = _serviceQueries.GetServiceById(id);
            ServiceModel.CheckIfUserAuthorizeToUnsubscribe(service, userId, ErrorMsgCannotUnsubscribe);

            var subscriptionId = SubscriptionQueries.GetSubscriptionIdFromServiceId(id);
            if (subscriptionId == 0) throw new System.Web.Http.HttpResponseException(HttpStatusCode.BadRequest);
            return SubscriptionQueries.Unsubscribe(subscriptionId);
        }

        public IActionResult Edit(string id)
        {
            if (id == null) throw new System.Web.Http.HttpResponseException(HttpStatusCode.BadRequest);

            var userId = Convert.ToUInt32(User.FindFirstValue(CustomClaimTypes.Identifier), 10);
            Service service = _serviceQueries.GetServiceById(Convert.ToUInt32(id, 10));
            ServiceModel.CheckIfUserAuthorizeToEdit(service, userId, ErrorMsgCannotEdit);

            IEnumerable<EnvironmentModel> environmentModels = EnvironmentModel.GetAllEnvironments(EnvironmentQueries, Mapper, userId);
            ViewBag.Environments = environmentModels;

            var model = Mapper.Map<ServiceModel>(service);
            return View(model);
        }

        [HttpPost]
        public IActionResult Edit(string id, ServiceModel model)
        {
            if (id == null || model == null) throw new System.Web.Http.HttpResponseException(HttpStatusCode.BadRequest);

            var userId = Convert.ToUInt32(User.FindFirstValue(CustomClaimTypes.Identifier), 10);
            Service service = _serviceQueries.GetServiceById(Convert.ToUInt32(id, 10));
            ServiceModel.CheckIfUserAuthorizeToEdit(service, userId, ErrorMsgCannotEdit);

            if (ModelState.IsValid)
            {
                model.Url = GetCanonicalUrl(model.Url);

                var currentUrl = service.Url.ToString();
                if (currentUrl != model.Url && ServiceQueries.IsServiceUrlUsed(model.Url, userId))
                {
                    ModelState.AddModelError("Url", _localizer[ErrorMsgUrlExists]);
                }
            }

            if (ModelState.IsValid)
            {
                model.Update(Convert.ToUInt32(id, 10), ServiceQueries, Mapper);
                return RedirectToAction("Index", "Home");
            }
            else
            {
                IEnumerable<EnvironmentModel> environmentModels = EnvironmentModel.GetAllEnvironments(EnvironmentQueries, Mapper, userId);
                ViewBag.Environments = environmentModels;
                return View(model);
            }
        }

        public bool Remove(string id)
        {
            var userId = Convert.ToUInt32(User.FindFirstValue(CustomClaimTypes.Identifier), 10);
            Service service = _serviceQueries.GetServiceById(Convert.ToUInt32(id, 10));
            ServiceModel.CheckIfUserAuthorizeToRemove(service, userId, ErrorMsgCannotRemove);

            return ServiceModel.Remove(Convert.ToUInt32(id, 10), ServiceQueries);
        }

        public static string GetCanonicalUrl(string modelUrl)
        {
            if (modelUrl == null) throw new System.Web.Http.HttpResponseException(HttpStatusCode.BadRequest);

            if (modelUrl.EndsWith('/')) modelUrl = modelUrl.Remove(modelUrl.Length - 1, 1);
            
            return modelUrl;
        }
    }
}