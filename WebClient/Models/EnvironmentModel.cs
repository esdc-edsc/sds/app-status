﻿using AutoMapper;
using GoC.AppStatus.DBAccess.DataAccess;
using GoC.AppStatus.WebClient.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Net.Http;

namespace GoC.AppStatus.WebClient.Models
{
    public class EnvironmentModel
    {
        public uint Id { get; set; }
        [Required(ErrorMessageResourceType = typeof(EnvironmentsModelResources), ErrorMessageResourceName = "RequiredField")]
        [MinLength(3, ErrorMessageResourceType = typeof(EnvironmentsModelResources), ErrorMessageResourceName = "EnvTitleMinLen")]
        [RegularExpression(@"^[A-Za-z0-9-\s]+$", ErrorMessageResourceType = typeof(EnvironmentsModelResources), ErrorMessageResourceName = "InvalidCharacters")]
        public string Title { get; set; }
        [Required(ErrorMessageResourceType = typeof(EnvironmentsModelResources), ErrorMessageResourceName = "RequiredField")]
        public string Description { get; set; }
        [Required(ErrorMessageResourceType = typeof(EnvironmentsModelResources), ErrorMessageResourceName = "RequiredField")]
        public string Colour { get; set; }

        internal void Add(IEnvironmentQueries queries, IMapper mapper, uint userCreatedId)
        {
            if (queries == null) throw new ArgumentNullException(nameof(queries));
            if (mapper == null) throw new ArgumentNullException(nameof(mapper));
            if (userCreatedId == 0) throw new ArgumentNullException(nameof(userCreatedId));

            var environment = mapper.Map<DBAccess.DBEntities.Environment>(this);
            environment.CreatedByUserId = userCreatedId;
            queries.AddEnvironment(environment);
        }

        internal bool CheckForTitleDuplicate(IEnvironmentQueries queries, uint userId)
        {
            if (queries == null) throw new ArgumentNullException(nameof(queries));
            return queries.IsEnvTitlelUsed(Title, userId);
        }

        internal static void CheckIfUserAuthorizeToEdit(DBAccess.DBEntities.Environment environment, uint userId, string ErrorMsgCannotEdit)
        {
            if (environment.CreatedByUserId != userId)
            {
                var msg = new HttpResponseMessage(HttpStatusCode.Unauthorized) { ReasonPhrase = ErrorMsgCannotEdit };
                throw new System.Web.Http.HttpResponseException(msg);
            }
        }

        internal static void CheckIfUserAuthorizeToRemove(DBAccess.DBEntities.Environment environment, uint userId, string ErrorMsg)
        {
            if (environment.CreatedByUserId != userId)
            {
                var msg = new HttpResponseMessage(HttpStatusCode.Unauthorized) { ReasonPhrase = ErrorMsg };
                throw new System.Web.Http.HttpResponseException(msg);
            }
        }

        internal static IEnumerable<EnvironmentModel> GetAllEnvironments(IEnvironmentQueries queries, IMapper mapper, uint userId = 0)
        {
            if (queries == null) throw new ArgumentNullException(nameof(queries));
            if (mapper == null) throw new ArgumentNullException(nameof(mapper));

            return mapper.Map<IEnumerable<EnvironmentModel>>(queries.GetAllEnvironments(userId));
        }

        internal static bool Remove(uint id, IEnvironmentQueries queries)
        {
            if (queries == null) throw new ArgumentNullException(nameof(queries));

            return queries.RemoveEnvironment(id);
        }

        internal void Update(uint id, IEnvironmentQueries queries, IMapper mapper)
        {
            if (queries == null) throw new ArgumentNullException(nameof(queries));
            if (mapper == null) throw new ArgumentNullException(nameof(mapper));

            var environment = mapper.Map<DBAccess.DBEntities.Environment>(this);
            queries.UpdateEnvironment(id, environment);
        }

    }
}
