﻿namespace GoC.AppStatus.WebClient.Models
{
    public class ErrorModel
    {
        public string EnTitle { get; set; }
        public string EnMessage { get; set; }
        public string FrTitle { get; set; }
        public string FrMessage { get; set; }
    }
}
