﻿using System;

namespace GoC.AppStatus.WebClient.Models
{
    public class ServiceViewModel
    {
        public uint Id{ get; set; }
        public Uri Url { get; set; }
        public string Name { get; set; }
        public uint envId { get; set; }
        public string envColour { get; set; }
        public string envTitle { get; set; }
        public uint CreatedByUserId { get; set; }
        public DateTime DisabledDatetime { get; set; }
        public bool Subscribed { get; set; }
        public int CertExpiryDays { get; set; }
    }
}
