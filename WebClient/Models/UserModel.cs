﻿using AutoMapper;
using GoC.AppStatus.DBAccess.DataAccess;
using GoC.AppStatus.DBAccess.DBEntities;
using GoC.AppStatus.WebClient.Configs;
using GoC.AppStatus.WebClient.Resources;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace GoC.AppStatus.WebClient.Models
{

    public class UserModel
    {
        [Required(ErrorMessageResourceType = typeof(UsersModelResources), ErrorMessageResourceName = "RequiredField")]
        [RegularExpression(@"^[A-Za-z0-9-\s]+$", ErrorMessageResourceType = typeof(UsersModelResources), ErrorMessageResourceName = "InvalidCharacters")]
        public string ActualName { get; set; }

        [Required(ErrorMessageResourceType = typeof(UsersModelResources), ErrorMessageResourceName = "RequiredField")]
        [EmailAddress(ErrorMessageResourceType = typeof(UsersModelResources), ErrorMessageResourceName = "EmailNotValid")]
        [CheckEmailDomain(ErrorMessageResourceType = typeof(UsersModelResources), ErrorMessageResourceName = "InvalidDomain")]
        public string Email { get; set; }

        [Required(ErrorMessageResourceType = typeof(UsersModelResources), ErrorMessageResourceName = "RequiredField")]
        [MinLength(10, ErrorMessageResourceType = typeof(UsersModelResources), ErrorMessageResourceName = "PasswordMinLen")]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*#?&_-])[A-Za-z\d@$!%*#?&_-]+$", ErrorMessageResourceType = typeof(UsersModelResources), ErrorMessageResourceName = "PasswordStrength")]
        public string Password { get; set; }

        [Required(ErrorMessageResourceType = typeof(UsersModelResources), ErrorMessageResourceName = "RequiredField")]
        [Compare("Password", ErrorMessageResourceType = typeof(UsersModelResources), ErrorMessageResourceName = "ConfirmPassword")]
        public string ConfirmPassword { get; set; }


        public bool CheckEmailDuplicate(IUserQueries queries)
        {
            if (queries == null) throw new ArgumentNullException(nameof(queries));
            return queries.IsUserEmailUsed(Email);            
        }

        internal User Add(IUserQueries queries, IMapper mapper)
        {
            if (queries == null) throw new ArgumentNullException(nameof(queries));
            if (mapper == null) throw new ArgumentNullException(nameof(mapper));

            var user = mapper.Map<User>(this);
            user.HashPassword();
            user.Id = queries.AddUser(user);
            return user;
        }
    }

    public class CheckEmailDomainAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, System.ComponentModel.DataAnnotations.ValidationContext validationContext)
        {
            if (validationContext != null && value != null)
            {
                var serviceOptions = (IOptions<AllowedEmailDomains>)
                validationContext.GetService(typeof(IOptions<AllowedEmailDomains>));

                if (serviceOptions != null)
                {
                    var service = serviceOptions.Value;
                    string domain = value.ToString().Split("@")[1];
                    if (!service.Domain.Any(domain.Contains))
                    {
                        return new ValidationResult(ErrorMessage);
                    }
                }
            }
            return ValidationResult.Success;
        }
    }

    public class AllowedEmailDomains
    {
#pragma warning disable CA2227 // Collection properties should be read only
        public List<string> Domain { get; set; }
#pragma warning restore CA2227 // Collection properties should be read only
    }
}
