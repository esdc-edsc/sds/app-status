﻿using GoC.AppStatus.DBAccess.DBEntities;
using System.Collections.Generic;


namespace GoC.AppStatus.WebClient.Models
{
    public class DashboardViewModel
    {
        public IEnumerable<ServiceViewModel> AllServices { get; set; }
    }
}
