﻿using AutoMapper;
using AutoMapper.Configuration.Conventions;
using GoC.AppStatus.DBAccess.DBEntities;
using GoC.AppStatus.DBAccess.DataAccess;
using GoC.AppStatus.WebClient.Configs;
using GoC.AppStatus.WebClient.Resources;
using System;
using System.ComponentModel.DataAnnotations;

namespace GoC.AppStatus.WebClient.Models
{

    public class UserSignInModel
    {
        [MapTo("Email")]
        [Required(ErrorMessageResourceType = typeof(UsersModelResources), ErrorMessageResourceName = "RequiredField")]
        public string SignInEmail { get; set; }

        [MapTo("Password")]
        [Required(ErrorMessageResourceType = typeof(UsersModelResources), ErrorMessageResourceName = "RequiredField")]
        public string SignInPassword { get; set; }

        public User Authenticate(IUserQueries queries, IMapper mapper)
        {
            if (queries == null) throw new ArgumentNullException(nameof(queries));
            if (mapper == null) throw new ArgumentNullException(nameof(mapper));

            if (string.IsNullOrEmpty(SignInEmail) || string.IsNullOrEmpty(SignInPassword)) return null;

            var dbUser = queries.GetUserByEmail(SignInEmail);
            if (dbUser == null) return null;

            var salt = dbUser.Password.Substring(0, dbUser.Password.IndexOf('.', StringComparison.Ordinal));
            var user = mapper.Map<User>(this);
            user.HashPassword(Convert.FromBase64String(salt));

            if (user.Password != dbUser.Password) return null;

            return dbUser;
        }
    }
}
