﻿using System;
using GoC.AppStatus.WebClient.Resources;
using System.ComponentModel.DataAnnotations;
using GoC.AppStatus.DBAccess.DataAccess;
using AutoMapper;
using GoC.AppStatus.DBAccess.DBEntities;
using System.Net.Http;
using System.Net;

namespace GoC.AppStatus.WebClient.Models
{
    public class ServiceModel
    {
        [Required(ErrorMessageResourceType = typeof(ServicesModelResources), ErrorMessageResourceName = "RequiredField")]
        [Url(ErrorMessageResourceType = typeof(ServicesModelResources), ErrorMessageResourceName = "InvalidUrl")]
#pragma warning disable CA1056 // Uri properties should not be strings
        public string Url { get; set; }
#pragma warning restore CA1056 // Uri properties should not be strings

        [Required(ErrorMessageResourceType = typeof(ServicesModelResources), ErrorMessageResourceName = "RequiredField")]
        [MinLength(6, ErrorMessageResourceType = typeof(ServicesModelResources), ErrorMessageResourceName = "NameMinLen")]
        [RegularExpression(@"^[A-Za-z0-9-\s]+$", ErrorMessageResourceType = typeof(ServicesModelResources), ErrorMessageResourceName = "InvalidCharacters")]
        public string Name { get; set; }
        public uint EnvironmentId { get; set; }

        internal bool CheckForNameDuplicate(IServiceQueries queries, uint userId)
        {
            if (queries == null) throw new ArgumentNullException(nameof(queries));
            return queries.IsServiceNameUsed(Name, userId);
        }

        internal bool CheckForUrlDuplicate(IServiceQueries queries, uint userId)
        {
            if (queries == null) throw new ArgumentNullException(nameof(queries));
            return queries.IsServiceUrlUsed(Url, userId);
        }

        internal void Add(IServiceQueries queries, IMapper mapper, uint userCreatedId)
        {
            if (queries == null) throw new ArgumentNullException(nameof(queries));
            if (mapper == null) throw new ArgumentNullException(nameof(mapper));
            if (userCreatedId == 0) throw new ArgumentNullException(nameof(userCreatedId));

            var service = mapper.Map<Service>(this);
            service.CreatedByUserId = userCreatedId;
            queries.AddService(service);
        }

        internal void Update(uint id, IServiceQueries queries, IMapper mapper)
        {
            if (queries == null) throw new ArgumentNullException(nameof(queries));
            if (mapper == null) throw new ArgumentNullException(nameof(mapper));

            var service = mapper.Map<Service>(this);
            queries.UpdateService(id, service);
        }

        internal static void CheckIfUserAuthorizeToEdit(Service service, uint userId, string ErrorMsgCannotEdit)
        {
            if (service.CreatedByUserId != userId)
            {
                var msg = new HttpResponseMessage(HttpStatusCode.Unauthorized) { ReasonPhrase = ErrorMsgCannotEdit };
                throw new System.Web.Http.HttpResponseException(msg);
            }
        }

        internal static void CheckIfUserAuthorizeToRemoveSubscribeUnsubscribe(Service service, uint userId, string ErrorMsg)
        {
            if (service.CreatedByUserId != userId)
            {
                var msg = new HttpResponseMessage(HttpStatusCode.Unauthorized) { ReasonPhrase = ErrorMsg };
                throw new System.Web.Http.HttpResponseException(msg);
            }
        }

        internal static void CheckIfUserAuthorizeToRemove(Service service, uint userId, string ErrorMsgCannotRemove)
        {
            CheckIfUserAuthorizeToRemoveSubscribeUnsubscribe(service, userId, ErrorMsgCannotRemove);
        }

        internal static void CheckIfUserAuthorizeToSubscribe(Service service, uint userId, string ErrorMsgCannotSubscribe)
        {
            CheckIfUserAuthorizeToRemoveSubscribeUnsubscribe(service, userId, ErrorMsgCannotSubscribe);
        }

        internal static void CheckIfUserAuthorizeToUnsubscribe(Service service, uint userId, string ErrorMsgCannotUnsubscribe)
        {
            CheckIfUserAuthorizeToRemoveSubscribeUnsubscribe(service, userId, ErrorMsgCannotUnsubscribe);
        }

        internal static bool Remove(uint id, IServiceQueries queries)
        {
            if (queries == null) throw new ArgumentNullException(nameof(queries));

            return queries.RemoveService(id);
        }
    }

    public class UrlAttribute : ValidationAttribute
    {
        public UrlAttribute() { }

        public override bool IsValid(object value)
        {
#pragma warning disable CA1062 // Validate arguments of public methods
            bool result = Uri.TryCreate(value.ToString(), UriKind.Absolute, out Uri uriResult)
                && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);

            return result;
#pragma warning restore CA1062 // Validate arguments of public methods
        }
    }
}
