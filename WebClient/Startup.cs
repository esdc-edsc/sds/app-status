﻿using AutoMapper;
using GoC.AppStatus.CheckService.Utilities;
using GoC.AppStatus.DBAccess.DataAccess;
using GoC.AppStatus.WebClient.Models;
using GoC.WebTemplate.Components.Core.Services;
using Hangfire;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using System;
using System.IO;

namespace GoC.AppStatus.WebClient
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            services.AddDistributedMemoryCache();
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(15);
                options.Cookie.Name = ".GoCWebTemplate.Session";
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
            });

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie();

            services.AddAutoMapper(typeof(AutoMapperProfile).Assembly);
            services.AddHangfire(); //custom function with configuration

            //DotNetTemplates Setup
            services.AddModelAccessor();
            services.ConfigureGoCTemplateRequestLocalization();

            services.AddMvc()
                .AddViewLocalization(
                    LanguageViewLocationExpanderFormat.Suffix,
                    opts => { opts.ResourcesPath = "Resources"; })
                .AddDataAnnotationsLocalization()
                //.SetCompatibilityVersion(CompatibilityVersion.Version_2_1); // core 2.1
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0); // core 3.1

            services.AddMvc(option => option.EnableEndpointRouting = false); // core 3.1

            //set configurations that need to use injection
            services.Configure<AllowedEmailDomains>(Configuration.GetSection("AllowedEmailDomains"));

            services.AddHttpClient<IServiceTester, ServiceTester>();

            //set concrete definitions for interface injection
            //following must be defined for hangfire to execute
            services.AddSingleton<IServiceManager, ServiceManager>();
            services.AddSingleton<IServiceTester, ServiceTester>();
            services.AddSingleton<IGCNotifyService, GcNotifyService>();
            services.AddSingleton<ISQLAccess, SQLAccess>();
            services.AddSingleton<IUserQueries, Queries>();
            services.AddSingleton<IServiceQueries, Queries>();
            services.AddSingleton<ISubscriptionQueries, Queries>();
            services.AddSingleton<IEnvironmentQueries, Queries>();

            // optional (gives additional messages for debug purpose [requires Package Microsoft.ApplicationInsights.AspNetCore]):
            //services.AddApplicationInsightsTelemetry();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        //public static void Configure(IApplicationBuilder app, IHostingEnvironment env) // core 2.1
        public static void Configure(IApplicationBuilder app, IHostEnvironment env) // core 3.1
        {
            if (app == null) throw new ArgumentNullException(nameof(app));

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseStatusCodePagesWithReExecute("/Error/{0}");
                app.UseHsts();
            }

            app.UseHangfireDashboard();

            var options = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(options.Value); //order matters here, must be before cookies and session for translation to work
            app.UseHttpsRedirection();
            //app.UseStaticFiles(); //needed if wwwroot folder is added
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), "Static")),
                RequestPath = "/Static"
            });
            app.UseCookiePolicy();
            app.UseSession();
            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            HangfireManager.CreateStatusCheckJobs();
        }
    }
}
