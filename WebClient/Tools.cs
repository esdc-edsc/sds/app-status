using System;

namespace GoC.AppStatus.WebClient
{
    public static class Tools
    {
        /// <summary>
        /// Initiates a web request to the specified URL and retrieves the number of days until the TLS (HTTPS) certificate expires.
        /// </summary>
        /// <param name="checkTargetUrl">The URL for which to retrieve expiry information.</param>
        /// <returns>
        /// The numbers of days (from current time) until the URL's TLS certificate expires. 
        /// A value of &lt;= 0 indicates the certificate is already expired.
        /// If the URL's protocol is not HTTPS, int.MaxValue is returned.
        /// </returns>
        /// <remarks>
        /// Requires .NET Framework 4.7+ or .NET Core 2.1+
        /// Requires the assembly references System.Net.Http
        /// </remarks>
        public static int GetUrlCertificateExpiryDays(Uri checkTargetUrl)
        {
            if (checkTargetUrl == null) throw new NullReferenceException("A checkTargetUrl must be specified");

            if (checkTargetUrl.Scheme == Uri.UriSchemeHttps)
            {
                try
                {
                    var requestHandler = new System.Net.Http.HttpClientHandler();
                    DateTime? certExpiry = null;

                    requestHandler.ServerCertificateCustomValidationCallback += (message, cert, chain, policyErrors) =>
                    {
                        certExpiry = cert?.NotAfter;
                        //returning true blindly (ie trusting everyone automatically) is normally very bad,
                        //but for the purposes of only checking certificate info, it'll be ok.
                        return true;
                    };

                    var client = new System.Net.Http.HttpClient(requestHandler);

                    client.GetAsync(checkTargetUrl).GetAwaiter().GetResult(); //don't really care about the response contents

                    var expiryDays = certExpiry?.Subtract(DateTime.Now).Days;
                    if (expiryDays == null)
                    {
                        throw new ApplicationException("Cannot verify expiry, a certificate was unexpectedly not found in the web response.");
                    }

                    return expiryDays ?? 0;
                }
                catch
                {
                    // NOTE: Optionally log errors 
                    throw;
                }
            }

            // If we get here, URL was not HTTPS
            return int.MaxValue;
        }

    }
}

