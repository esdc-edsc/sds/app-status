﻿using System.Configuration;

namespace GoC.AppStatus.WebClient.Configs
{
    public class AuthConfig : ConfigurationSection
    {
        [ConfigurationProperty("Method", IsRequired = true)]
        public AuthMethod Method
        {
            get { return (AuthMethod)this[nameof(Method)]; }
            set { this[nameof(Method)] = value; }
        }
    }
    public enum AuthMethod { SelfManaged, InternalAD }
}
