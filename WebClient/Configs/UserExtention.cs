﻿using GoC.AppStatus.DBAccess.DBEntities;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;
using System.Security.Cryptography;

namespace GoC.AppStatus.WebClient.Configs
{
    public static class UserExtention
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pending>")]
        public static void HashPassword(this User user, byte[] salt = null)
        {
            int iterations = 10000;

            //Generate salt if one is not provided
            if (salt == null)
            {
                salt = new byte[128 / 8];
                using (var rng = RandomNumberGenerator.Create())
                {
                    rng.GetBytes(salt);
                }
            }

            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: user.Password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: iterations,
                numBytesRequested: 256 / 8));

            user.Password = $"{Convert.ToBase64String(salt)}.{hashed}";

        }
    }
}
