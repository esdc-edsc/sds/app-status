﻿using Microsoft.Extensions.Configuration;

namespace GoC.AppStatus.WebClient.Configs
{
    public static class ConfigurationExtention
    {
        public static T BindObject<T>(this IConfiguration config, string key) where T : new()
        {
            var obj = new T();
            config.GetSection(key).Bind(obj);
            return obj;
        }
    }
}
