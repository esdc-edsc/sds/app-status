﻿using FluentAssertions;
using GoC.AppStatus.DBAccess.DBEntities;
using GoC.AppStatus.DBAccess.DataAccess;
using GoC.AppStatus.WebClient.Test;
using System;
using Xunit;

namespace GoC.AppStatus.DBAccess.Test
{
    public class ServiceQueriesTests
    {
        [Theory, AutoNSubstituteData]
        public void AddServiceRequiresService(Queries sut)
        {
            Service service = null;
            Action act = () => sut.AddService(service);
            act.Should().Throw<ArgumentNullException>();
        }

        [Theory, AutoNSubstituteData]
        public void UpdateServiceRequiresId(Queries sut, uint serviceId, Service service)
        {
            serviceId = 0;
            Action act = () => sut.UpdateService(serviceId, service);
            act.Should().Throw<ArgumentException>();
        }

        [Theory, AutoNSubstituteData]
        public void RemoveServiceRequiresId(Queries sut, uint serviceId)
        {
            serviceId = 0;
            Action act = () => sut.RemoveService(serviceId);
            act.Should().Throw<ArgumentException>();
        }

        [Theory, AutoNSubstituteData]
        public void GetActiveFailureReportRequiresId(Queries sut, uint serviceId)
        {
            serviceId = 0;
            Action act = () => sut.GetActiveFailureReport(serviceId);
            act.Should().Throw<ArgumentException>();
        }

        [Theory, AutoNSubstituteData]
        public void GetByIdRequiresId(Queries sut, uint serviceId)
        {
            serviceId = 0;
            Action act = () => sut.GetServiceById(serviceId);
            act.Should().Throw<ArgumentException>();
        }

        [Theory, AutoNSubstituteData]
        public void GetSubscribedUsersRequiresService(Queries sut)
        {
            Service service = null;
            Action act = () => sut.GetSubscribedUsers(service);
            act.Should().Throw<ArgumentNullException>();
        }

        [Theory, AutoNSubstituteData]
        public void GetSubscriptionIdFromServiceIdRequiresId(Queries sut, uint serviceId)
        {
            serviceId = 0;
            Action act = () => sut.GetSubscriptionIdFromServiceId(serviceId);
            act.Should().Throw<ArgumentException>();
        }

        [Theory, AutoNSubstituteData]
        public void RemoveSubscriptionRequiresId(Queries sut, uint serviceId)
        {
            serviceId = 0;
            Action act = () => sut.RemoveSubscription(serviceId);
            act.Should().Throw<ArgumentException>();
        }

        [Theory, AutoNSubstituteData]
        public void SetSubscriptionStatusRequiresId(Queries sut, uint subscriptionId)
        {
            subscriptionId = 0;
            Action act = () => sut.SetSubscriptionStatus(subscriptionId, false);
            act.Should().Throw<ArgumentException>();
        }

        [Theory, AutoNSubstituteData]
        public void ReportFailedServiceResolvedRequiresReport(Queries sut)
        {
            ServiceFailureReport serviceFailureReport = null;
            Action act = () => sut.ReportFailedServiceResolved(serviceFailureReport);
            act.Should().Throw<ArgumentNullException>();
        }

        [Theory, AutoNSubstituteData]
        public void ReportFailedServiceRequiresReport(Queries sut)
        {
            ServiceFailureReport serviceFailureReport = null;
            Action act = () => sut.ReportFailedService(serviceFailureReport);
            act.Should().Throw<ArgumentNullException>();
        }
    }
}
