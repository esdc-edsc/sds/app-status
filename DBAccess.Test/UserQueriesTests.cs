﻿using FluentAssertions;
using GoC.AppStatus.DBAccess.DBEntities;
using GoC.AppStatus.DBAccess.DataAccess;
using GoC.AppStatus.WebClient.Test;
using System;
using Xunit;

namespace GoC.AppStatus.DBAccess.Test
{
    public class UserQueriesTests
    {
        [Theory, AutoNSubstituteData]
        public void AddUserRequiresUser(Queries sut)
        {
            User user = null;
            Action act = () => sut.AddUser(user);
            act.Should().Throw<ArgumentNullException>();
        }

        [Theory, AutoNSubstituteData]
        public void GetUserByEmailRequiresEmail(Queries sut)
        {
            string email = null;
            Action act = () => sut.GetUserByEmail(email);
            act.Should().Throw<ArgumentNullException>();
        }

        [Theory, AutoNSubstituteData]
        public void IsUserEmailUsedRequiresEmail(Queries sut)
        {
            string email = null;
            Action act = () => sut.IsUserEmailUsed(email);
            act.Should().Throw<ArgumentNullException>();
        }
    }
}
