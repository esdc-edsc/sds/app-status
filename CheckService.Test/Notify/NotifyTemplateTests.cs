﻿using FluentAssertions;
using GoC.AppStatus.CheckService.NotifyTemplates;
using Xunit;

namespace GoC.AppStatus.CheckService.Test.Notify
{
    public class NotifyTemplateTests
    {
        [Fact]
        public void TemplateCreatesWithSiteUrlByDefault()
        {
            var sut = new ExampleTest();
            sut.Personalisation.Should().ContainValue("////localhost:5000//");
        }
    }
}
