﻿using FluentAssertions;
using GoC.AppStatus.CheckService.Utilities;
using System;
using System.Collections.Generic;
using Xunit;
using GoC.AppStatus.DBAccess.DBEntities;

namespace GoC.AppStatus.CheckService.Test.Utility
{
    public class ServiceManagerTests
    {
        [Theory, AutoNSubstituteData]
        public void ReportFailedServicesDoesntThrowErrorsWhenEmpty(ServiceManager sut)
        {
            Action act = () => sut.ReportFailedServices(null);
            act.Should().NotThrow<Exception>();
        }

        [Theory, AutoNSubstituteData]
        public void ReportFailedServicesExecutesWithoutErrors(ServiceManager sut, List<Service> services)
        {
            Action act = () => sut.ReportFailedServices(services);
            act.Should().NotThrow<Exception>();
        }

        [Theory, AutoNSubstituteData]
        public void NotifyFailedServicesThrowsExeptionWhenEmpty(ServiceManager sut)
        {
            Action act = () => sut.NotifySubscribersOfFailedService(null, null);
            act.Should().Throw<ArgumentNullException>().WithMessage("*(Parameter 'service')");
        }

        [Theory, AutoNSubstituteData]
        public void NotifyFailedServicesExecutesWithoutErrors(ServiceManager sut, IEnumerable<User> users, Service service)
        {
            Action act = () => sut.NotifySubscribersOfFailedService(users, service);
            act.Should().NotThrow<Exception>();
        }

        [Theory, AutoNSubstituteData]
        public void GetSubscribedUsersExecutesWithoutErrors(ServiceManager sut, Service service)
        {
            Action act =  () => sut.GetSubscribedUsers(service);
            act.Should().NotThrow<Exception>();
        }
    }
}
