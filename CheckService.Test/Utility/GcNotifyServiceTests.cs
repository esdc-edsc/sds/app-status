﻿using FluentAssertions;
using GoC.AppStatus.CheckService.Utilities;
using Microsoft.Extensions.Configuration;
using System;
using Xunit;

namespace GoC.AppStatus.CheckService.Test.Utility
{
    public class GcNotifyServiceTests
    {
        [Theory, AutoNSubstituteData]
        public void SuccessfullyCreateNotifyClient(IConfigurationRoot config)
        {
            GcNotifyService sut = null;
            Action act = () => sut = new GcNotifyService(config);

            act.Should().NotThrow<Exception>();
            sut.Should().NotBeNull();
        }
    }
}
