using FluentAssertions;
using GoC.AppStatus.CheckService.Utilities;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace GoC.AppStatus.CheckService.Test.Utility
{
    public class ServiceTesterTests
    {
        [Theory, AutoNSubstituteData]
        public void GetStatusCodeReturnsBadRequest(ServiceTester sut)
        {
            var code = sut.GetStatusCode((Uri)null);
            code.Should().Be(HttpStatusCode.BadRequest);
        }

        [Fact]
        public void GetStatusCodeReturnsNotFound()
        {
            // Arrange:
            var messageHandler = new MockHttpMessageHandler("", HttpStatusCode.NotFound);
            var httpClient = new HttpClient(messageHandler);
            ServiceTester tester = new ServiceTester(httpClient);

            // Act:
            HttpStatusCode code = tester.GetStatusCode(new Uri("http://whatever.com"));
            httpClient.Dispose();
            messageHandler.Dispose();

            // Assert:
            code.Should().Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public void GetStatusCodeReturnsOK()
        {
            // Arrange:
            var messageHandler = new MockHttpMessageHandler("", HttpStatusCode.OK);
            var httpClient = new HttpClient(messageHandler);
            ServiceTester tester = new ServiceTester(httpClient);

            // Act:
            HttpStatusCode code = tester.GetStatusCode(new Uri("http://whatever.com"));
            httpClient.Dispose();
            messageHandler.Dispose();

            // Assert:
            code.Should().Be(HttpStatusCode.OK);
        }

    }
}
