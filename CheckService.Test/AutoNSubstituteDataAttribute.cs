﻿using AutoFixture;
using AutoFixture.AutoNSubstitute;
using AutoFixture.Xunit2;
using GoC.AppStatus.DBAccess.DBEntities;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace GoC.AppStatus.CheckService.Test
{
    internal class AutoNSubstituteDataAttribute : AutoDataAttribute
    {
        public AutoNSubstituteDataAttribute()
            : base(() => new Fixture().Customize(new CheckServiceCustomization()))
        { }
    }

    public class CheckServiceCustomization : CompositeCustomization
    {
        public CheckServiceCustomization()
            : base(new CoreCustomization(), new AutoNSubstituteCustomization())
        { }
    }

    public class CoreCustomization : ICustomization
    {
        public void Customize(IFixture fixture)
        {
            fixture.Register<IConfigurationRoot>(() => new ConfigurationBuilder()
                .AddJsonFile(Path.Combine(Directory.GetCurrentDirectory(), "..\\..\\..\\appsettings.json"), optional: false, reloadOnChange: true)
                .Build());
            fixture.Customize<User>(c => c.With(p => p.Email, "fake@email"));
        }
    }
}
