# App Status

An application service to track the status of any other application, controlled by a web application front end.

The application will allow any pre-authenticated ESDC employee, through signle-sign-on, to _register_ a _service_ for monitoring.
Any user could then _subscribe_ to any _registered service_.
By _subscribing_ the user will recive an email notice any time the _service_ is found to be non-responsive.

Please view the [project wiki](https://gitlab.com/esdc-edsc/sds/app-status/-/wikis/home) for more details on how each of the components works and its use of any 3-rd party tools.

## Contributing

We love to see anyone using the application contributing back to it by creating bugs, and feature requests through issues or helping the development by submitting Merge Requests! Please review the [CONTRIBUTING.md](CONTRIBUTING.md) for more details.

## License

Unless otherwise noted, the whole of this project is covered under [Crown Copyright, Government of Canada](LICENSE). Allowing for all Government of Canada employees to contribute to this project.

The Canada wordmark and related graphics associated with this distribution are protected under trademark law and copyright law. No permission is granted to use them outside the parameters of the Government of Canada's corporate identity program. For more information, see [Federal identity requirements](https://www.canada.ca/en/treasury-board-secretariat/topics/government-communications/federal-identity-requirements.html).
