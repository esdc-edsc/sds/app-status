﻿using FluentAssertions;
using GoC.AppStatus.DBAccess.DBEntities;
using GoC.AppStatus.WebClient.Configs;
using System;
using Xunit;

namespace GoC.AppStatus.WebClient.Test.General
{
    public class ExtensionTests
    {
        [Theory, AutoNSubstituteData]
        public void PasswordHashWorks(User sut)
        {
            var unhashedPassword = sut.Password;
            sut.HashPassword();
            sut.Password.Should().NotBe(unhashedPassword);
        }

        [Theory, AutoNSubstituteData]
        public void PasswordHashWithSaltReturnsSameHash(User sut)
        {
            var unhashedPassword = sut.Password;
            byte[] salt = Convert.FromBase64String("ThisIsACustomSalt123");

            sut.HashPassword(salt);
            sut.Password.Should().NotBe(unhashedPassword);
            var hashedPassword = sut.Password;
            sut.Password = unhashedPassword;
            sut.HashPassword(salt);

            sut.Password.Should().Be(hashedPassword);
        }
    }
}
