﻿using AutoFixture.Xunit2;
using FluentAssertions;
using GoC.AppStatus.CheckService.Utilities;
using GoC.AppStatus.DBAccess.DBEntities;
using GoC.AppStatus.DBAccess.DataAccess;
using GoC.AppStatus.WebClient.Configs;
using GoC.AppStatus.WebClient.Controllers;
using GoC.AppStatus.WebClient.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using NSubstitute;
using System;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Web.Http;
using Xunit;
using System.Globalization;

namespace GoC.AppStatus.WebClient.Test.ControllerTests
{
    public class ServicesControllerTests
    {
        [Theory, AutoNSubstituteData]
        public void RegisterReturnsAView(ServicesController sut)
        {
            var result = sut.Register();
            result.Should().BeOfType<ViewResult>();
        }


        [Theory, AutoNSubstituteData]
        public void RegisterPostHappyPath([Frozen] IServiceQueries serviceQueries, [Frozen] IServiceTester serviceTester, ServicesController sut, ServiceModel model)
        {
            serviceQueries.IsServiceUrlUsed(default, default).ReturnsForAnyArgs(false);
            serviceTester.IsStatusCodeValid(default).ReturnsForAnyArgs(true);
            //tests needs a user ID to run
            sut.User.FindFirst(CustomClaimTypes.Identifier).Returns(new Claim(CustomClaimTypes.Identifier, "1"));

            var result = sut.Register(model);

            result.Should().BeOfType<RedirectToActionResult>();
        }

        [Theory, AutoNSubstituteData]
        public void RegisterUrlDoesntReturnValidResponse([Frozen] IServiceQueries serviceQueries, [Frozen] IServiceTester serviceTester, [Frozen] IStringLocalizer<ServicesController> localizer, ServicesController sut, ServiceModel model)
        {
            serviceQueries.IsServiceUrlUsed(default, default).ReturnsForAnyArgs(false);
            serviceTester.IsStatusCodeValid(default).ReturnsForAnyArgs(false);
            localizer[ServicesController.ErrorMsgUrlDoesntReturnValidResponse].Returns(new LocalizedString(ServicesController.ErrorMsgUrlDoesntReturnValidResponse, ServicesController.ErrorMsgUrlDoesntReturnValidResponse));

            sut.Register(model);

            sut.ModelState.ErrorCount.Should().Be(1);
            sut.ModelState.Values.First().Errors.First().ErrorMessage.Should().Be(ServicesController.ErrorMsgUrlDoesntReturnValidResponse);
        }

        [Theory, AutoNSubstituteData]
        public void RegisterNameExists([Frozen] IServiceQueries serviceQueries, [Frozen] IStringLocalizer<ServicesController> localizer, ServicesController sut, ServiceModel model)
        {
            serviceQueries.IsServiceNameUsed(default, default).ReturnsForAnyArgs(true);
            localizer[ServicesController.ErrorMsgNameExists].Returns(new LocalizedString(ServicesController.ErrorMsgNameExists, ServicesController.ErrorMsgNameExists));

            sut.Register(model);

            sut.ModelState.ErrorCount.Should().Be(1);
            sut.ModelState.Values.First().Errors.First().ErrorMessage.Should().Be(ServicesController.ErrorMsgNameExists);
        }

        [Theory, AutoNSubstituteData]
        public void RegisterUrlExists([Frozen] IServiceQueries serviceQueries, [Frozen] IServiceTester serviceTester, [Frozen] IStringLocalizer<ServicesController> localizer, ServicesController sut, ServiceModel model)
        {
            serviceQueries.IsServiceUrlUsed(default, default).ReturnsForAnyArgs(true);
            serviceTester.IsStatusCodeValid(default).ReturnsForAnyArgs(true);
            localizer[ServicesController.ErrorMsgUrlExists].Returns(new LocalizedString(ServicesController.ErrorMsgUrlExists, ServicesController.ErrorMsgUrlExists));

            sut.Register(model);

            sut.ModelState.ErrorCount.Should().Be(1);
            sut.ModelState.Values.First().Errors.First().ErrorMessage.Should().Be(ServicesController.ErrorMsgUrlExists);
        }

        [Theory, AutoNSubstituteData]
        public void EditPostHappyPath([Frozen] IServiceQueries serviceQueries, ServicesController sut, uint serviceId, ServiceModel model)
        {
            uint userId = 1;
            Service service = new Service { Id = serviceId, CreatedByUserId = userId, Url = model.Url };
            serviceQueries.GetServiceById(serviceId).Returns(service);
            sut.User.FindFirst(CustomClaimTypes.Identifier).Returns(new Claim(CustomClaimTypes.Identifier, userId.ToString(CultureInfo.InvariantCulture)));

            var result = sut.Edit(serviceId.ToString(CultureInfo.InvariantCulture), model);
            result.Should().BeOfType<RedirectToActionResult>();
        }

        [Theory, AutoNSubstituteData]
        public void EditReturnsUnauthorizedResponse([Frozen]IServiceQueries serviceQueries, ServicesController sut)
        {
            uint serviceId = 1;
            uint userId = 2;
            serviceQueries.GetServiceById(serviceId).Returns(new Service { Id = serviceId, CreatedByUserId = 3 });
            sut.User.FindFirst(CustomClaimTypes.Identifier).Returns(new Claim(CustomClaimTypes.Identifier, userId.ToString(CultureInfo.InvariantCulture)));

            Action act = () => sut.Edit(serviceId.ToString(CultureInfo.InvariantCulture));
            act.Should().Throw<HttpResponseException>().Equals(HttpStatusCode.Unauthorized);
        }

        [Theory, AutoNSubstituteData]
        public void EditReturnsAView([Frozen] IServiceQueries serviceQueries, ServicesController sut)
        {
            uint serviceId = 1;
            uint userId = 2;
            serviceQueries.GetServiceById(serviceId).Returns(new Service { Id = serviceId, CreatedByUserId = userId });
            sut.User.FindFirst(CustomClaimTypes.Identifier).Returns(new Claim(CustomClaimTypes.Identifier, userId.ToString(CultureInfo.InvariantCulture)));

            var result = sut.Edit(serviceId.ToString(CultureInfo.InvariantCulture));
            result.Should().BeOfType<ViewResult>();
        }

        [Theory, AutoNSubstituteData]
        public void EditReturnsBadRequest(ServicesController sut)
        {
            Action act = () => sut.Edit(null);
            act.Should().Throw<HttpResponseException>().Equals(HttpStatusCode.BadRequest);
        }

        [Theory, AutoNSubstituteData]
        public void EditPostReturnsUnauthorizedResponse([Frozen] IServiceQueries serviceQueries, ServicesController sut, uint serviceId, ServiceModel model)
        {
            uint userId = 1;
            Service service = new Service { Id = serviceId, CreatedByUserId = userId, Url = model.Url };
            serviceQueries.GetServiceById(serviceId).Returns(service);
            sut.User.FindFirst(CustomClaimTypes.Identifier).Returns(new Claim(CustomClaimTypes.Identifier, "2"));

            Action act = () => sut.Edit(serviceId.ToString(CultureInfo.InvariantCulture));
            act.Should().Throw<HttpResponseException>().Equals(HttpStatusCode.Unauthorized);
        }

        [Theory, AutoNSubstituteData]
        public void EditUrlWithDuplicatFails([Frozen] IServiceQueries serviceQueries, [Frozen] IStringLocalizer<ServicesController> localizer, ServicesController sut, ServiceModel model)
        {
            model.Url = "http://changedToExistingUrl.com/";
            uint existingServiceId = 1;
            uint userId = 1;
            var existingService = new Service { Id = existingServiceId, CreatedByUserId = userId, Url = "http://currentUrl/" };
            serviceQueries.GetServiceById(existingServiceId).Returns(existingService);
            serviceQueries.IsServiceUrlUsed(ServicesController.GetCanonicalUrl(model.Url), 1).Returns(true);
            localizer[ServicesController.ErrorMsgUrlExists].Returns(new LocalizedString(ServicesController.ErrorMsgUrlExists, ServicesController.ErrorMsgUrlExists));
            sut.User.FindFirst(CustomClaimTypes.Identifier).Returns(new Claim(CustomClaimTypes.Identifier, userId.ToString(CultureInfo.InvariantCulture)));

            sut.Edit(existingServiceId.ToString(CultureInfo.InvariantCulture), model);

            sut.ModelState.ErrorCount.Should().Be(1);
            sut.ModelState.Values.First().Errors.First().ErrorMessage.Should().Be(ServicesController.ErrorMsgUrlExists);
        }

        [Theory, AutoNSubstituteData]
        public void EditUserHasNotCreatedService([Frozen] IServiceQueries serviceQueries, ServicesController sut)
        {
            uint existingServiceId = 1;
            var existingService = new Service { Id = existingServiceId, CreatedByUserId = 2, Url = "http://whatever.com/" };
            serviceQueries.GetServiceById(existingServiceId).Returns(existingService);
            sut.User.FindFirst(CustomClaimTypes.Identifier).Returns(new Claim(CustomClaimTypes.Identifier, "1"));

            Action act = () => sut.Edit(existingServiceId.ToString(CultureInfo.InvariantCulture));

            act.Should().Throw<HttpResponseException>().Where(e => e.Response.ReasonPhrase.Equals(ServicesController.ErrorMsgCannotEdit, StringComparison.OrdinalIgnoreCase));
        }

        [Theory, AutoNSubstituteData]
        public void RemoveReturnsBadRequest(ServicesController sut)
        {
            Action act = () => sut.Edit(null);
            act.Should().Throw<System.Web.Http.HttpResponseException>().Equals(HttpStatusCode.BadRequest);
        }

        [Theory, AutoNSubstituteData]
        public void RemoveUserHasNotCreatedService([Frozen] IServiceQueries serviceQueries, ServicesController sut)
        {
            uint existingServiceId = 1;
            var existingService = new Service { Id = existingServiceId, CreatedByUserId = 2, Url = "http://whatever.com/" };
            serviceQueries.GetServiceById(existingServiceId).Returns(existingService);
            sut.User.FindFirst(CustomClaimTypes.Identifier).Returns(new Claim(CustomClaimTypes.Identifier, "1"));

            Action act = () => sut.Remove(existingServiceId.ToString(CultureInfo.InvariantCulture));

            act.Should().Throw<HttpResponseException>().Where(e => e.Response.ReasonPhrase.Equals(ServicesController.ErrorMsgCannotRemove, StringComparison.OrdinalIgnoreCase));
        }

        [Theory, AutoNSubstituteData]
        public void SubscribeReturnsBadRequest(ServicesController sut)
        {
            Action act = () => sut.Subscribe(null);
            act.Should().Throw<HttpResponseException>().Equals(HttpStatusCode.BadRequest);
        }

        [Theory, AutoNSubstituteData]
        public void SubscribeUserHasNotCreatedService([Frozen] IServiceQueries serviceQueries, ServicesController sut)
        {
            uint existingServiceId = 1;
            var existingService = new Service { Id = existingServiceId, CreatedByUserId = 2, Url = "http://whatever.com/" };
            serviceQueries.GetServiceById(existingServiceId).Returns(existingService);
            sut.User.FindFirst(CustomClaimTypes.Identifier).Returns(new Claim(CustomClaimTypes.Identifier, "1"));

            Action act = () => sut.Subscribe(existingServiceId.ToString(CultureInfo.InvariantCulture));

            act.Should().Throw<HttpResponseException>().Where(e => e.Response.ReasonPhrase.Equals(ServicesController.ErrorMsgCannotSubscribe, StringComparison.OrdinalIgnoreCase));
        }

        [Theory, AutoNSubstituteData]
        public void UnsubscribeReturnsBadRequest(ServicesController sut)
        {
            Action act = () => sut.Unsubscribe(null);
            act.Should().Throw<HttpResponseException>().Equals(HttpStatusCode.BadRequest);
        }

        [Theory, AutoNSubstituteData]
        public void UnsubscribeUserHasNotCreatedService([Frozen] IServiceQueries serviceQueries, ServicesController sut)
        {
            uint existingServiceId = 1;
            var existingService = new Service { Id = existingServiceId, CreatedByUserId = 2, Url = "http://whatever.com/" };
            serviceQueries.GetServiceById(existingServiceId).Returns(existingService);
            sut.User.FindFirst(CustomClaimTypes.Identifier).Returns(new Claim(CustomClaimTypes.Identifier, "1"));

            Action act = () => sut.Unsubscribe(existingServiceId.ToString(CultureInfo.InvariantCulture));

            act.Should().Throw<HttpResponseException>().Where(e => e.Response.ReasonPhrase.Equals(ServicesController.ErrorMsgCannotUnsubscribe, StringComparison.OrdinalIgnoreCase));
        }
    }
}
