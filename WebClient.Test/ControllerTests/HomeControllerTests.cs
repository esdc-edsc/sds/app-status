using FluentAssertions;
using GoC.AppStatus.WebClient.Configs;
using GoC.AppStatus.WebClient.Controllers;
using GoC.WebTemplate.Components.Entities;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using System.Collections.Generic;
using System.Security.Claims;
using Xunit;

namespace GoC.AppStatus.WebClient.Test.ControllerTests
{
    public class HomeControllerTests
    {
        [Theory, AutoNSubstituteData]
        public void IndexReturnsAView(HomeController sut)
        {
            var result = sut.Index();
            result.Should().BeOfType<ViewResult>();
        }

        [Theory, AutoNSubstituteData]
        public void ValidUserCallingIndexReturnsDashboardView(HomeController sut)
        {
            //Force user to be valid
            sut.User.Identity.IsAuthenticated.Returns(true);
            sut.User.FindFirst(CustomClaimTypes.Identifier).Returns(new Claim(CustomClaimTypes.Identifier, "10"));

            var result = sut.Index();
            ((ViewResult)result).ViewName.Should().Be("Dashboard");
        }

        [Theory, AutoNSubstituteData]
        public void InvalidUserCallingIndexReturnsPreviewView(HomeController sut)
        {
            sut.User.Identity.IsAuthenticated.Returns(false); //force user to be invalid
            var result = sut.Index();
            ((ViewResult)result).ViewName.Should().Be("Preview");
        }

        [Theory, AutoNSubstituteData]
        public void ValidUserCallingGetMenuReturnsNotEmptyMenu(HomeController sut)
        {
            // Arrange:
            sut.User.Identity.IsAuthenticated.Returns(true); //force user to be valid

            // Act:
            List<MenuLink> menu = sut.GetMenu();

            // Assert:
            menu.Should().NotBeNull();
        }

        [Theory, AutoNSubstituteData]
        public void InvalidUserCallingGetMenuReturnsEmptyMenu(HomeController sut)
        {
            // Arrange:
            sut.User.Identity.IsAuthenticated.Returns(false); //force user to be invalid

            // Act:
            List<MenuLink> menu = sut.GetMenu();

            // Assert:
            menu.Should().BeNull();
        }
    }
}
