﻿using AutoFixture;
using AutoFixture.AutoNSubstitute;
using AutoFixture.Xunit2;
using GoC.AppStatus.WebClient.Controllers;
using GoC.AppStatus.WebClient.Models;
using AutoMapper;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace GoC.AppStatus.WebClient.Test
{
    internal class AutoNSubstituteDataAttribute : AutoDataAttribute
    {
        public AutoNSubstituteDataAttribute()
            : base(() => new Fixture().Customize(new AppStatusCustomization()))
        { }
    }

    public class AppStatusCustomization : CompositeCustomization
    {
        public AppStatusCustomization()
            : base(new CoreCustomization(), new AutoNSubstituteCustomization())
        { }
    }

    public class CoreCustomization : ICustomization
    {
        public void Customize(IFixture fixture)
        {
            fixture.Customize<UserModel>(
                c => c
                    .With(p => p.Email, "testuser@hrsdc-rhdcc.gc.ca")
                    .With(p => p.Password, "Password1!"));

            fixture.Customize<HomeController>(c => c.Without(p => p.ViewData));
            fixture.Customize<AccountController>(c => c.Without(p => p.ViewData));
            fixture.Customize<ServicesController>(c => c.Without(p => p.ViewData));

            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AutoMapperProfile>();
            });
            fixture.Customize<IMapper>(
                c => c.FromFactory(() => config.CreateMapper()));

            fixture.Customize<ServiceModel>(c => c
                    .With(p => p.Url, "https://www.cbc.ca"));

            fixture.Customize<BindingInfo>(c => c.OmitAutoProperties()); // core 3.1
        }
    }
}
