﻿using FluentAssertions;
using GoC.AppStatus.DBAccess.DBEntities;
using GoC.AppStatus.WebClient.Models;
using GoC.AppStatus.WebClient.Resources;
using GoC.AppStatus.WebClient.Test;
using System;
using System.ComponentModel.DataAnnotations;
using Xunit;

namespace WebClient.Test.ModelTests
{
    public class UsersModelTest
    {

        [Theory]
        [AutoNSubstituteData]
        public void TestSuccessCase(UserModel user)
        {
            user.ConfirmPassword = user.Password;
            ValidationContext context = new ValidationContext(user);
            Action act = () => Validator.ValidateObject(user, context, true);
            act.Should().NotThrow<Exception>();
        }

        [Theory]
        [AutoNSubstituteData]
        public void TestActualNameNull(UserModel user)
        {
            user.ActualName = null;
            ValidationContext context = new ValidationContext(user);

            Action act = () => Validator.ValidateObject(user, context, true);
            act.Should().Throw<ValidationException>().WithMessage(UsersModelResources.RequiredField);
        }

        [Theory]
        [AutoNSubstituteData]
        public void TestEmailNull(UserModel user)
        {
            user.Email = null;
            ValidationContext context = new ValidationContext(user);

            Action act = () => Validator.ValidateObject(user, context);
            act.Should().Throw<ValidationException>().WithMessage(UsersModelResources.RequiredField);
        }

        [Theory]
        [AutoNSubstituteData]
        public void TestPasswordCharSuccess(UserModel user)
        {
            user.Password = "Pa1@$!%*#?&_-";
            user.ConfirmPassword = "Pa1@$!%*#?&_-";
            ValidationContext context = new ValidationContext(user);

            Action act = () => Validator.ValidateObject(user, context, true);
            act.Should().NotThrow<Exception>();
        }

        [Theory]
        [AutoNSubstituteData]
        public void TestPasswordNull(UserModel user)
        {
            user.Password = null;
            user.ConfirmPassword = "Password1!";
            ValidationContext context = new ValidationContext(user);

            Action act = () => Validator.ValidateObject(user, context, true);
            act.Should().Throw<ValidationException>().WithMessage(UsersModelResources.RequiredField);
        }

        [Theory]
        [AutoNSubstituteData]
        public void TestConfirmPasswordNull(UserModel user)
        {
            user.ConfirmPassword = null;
            ValidationContext context = new ValidationContext(user);

            Action act = () => Validator.ValidateObject(user, context, true);
            act.Should().Throw<ValidationException>().WithMessage(UsersModelResources.RequiredField);
        }

        [Theory]
        [AutoNSubstituteData]
        public void TestInvalidCharActualName(UserModel user)
        {
            user.ActualName = "TestUser@@";
            ValidationContext context = new ValidationContext(user);

            Action act = () => Validator.ValidateObject(user, context, true);
            act.Should().Throw<ValidationException>().WithMessage(UsersModelResources.InvalidCharacters);
        }

        [Theory]
        [AutoNSubstituteData]
        public void TestCharPassword(UserModel user)
        {
            user.Password = "PasswordOne";
            user.ConfirmPassword = "PasswordOne";
            ValidationContext context = new ValidationContext(user);

            Action act = () => Validator.ValidateObject(user, context, true);
            act.Should().Throw<ValidationException>().WithMessage(UsersModelResources.PasswordStrength);
        }

        [Theory]
        [AutoNSubstituteData]
        public void TestMatchingPassword(UserModel user)
        {
            user.Password = "Password1!";
            user.ConfirmPassword = "Password";
            ValidationContext context = new ValidationContext(user);

            Action act = () => Validator.ValidateObject(user, context, true);
            act.Should().Throw<ValidationException>().WithMessage(UsersModelResources.ConfirmPassword);
        }

        [Theory]
        [AutoNSubstituteData]
        public void TestMinLenPassword(UserModel user)
        {
            user.Password = "Pass1!";
            user.ConfirmPassword = "Pass1!";
            ValidationContext context = new ValidationContext(user);

            Action act = () => Validator.ValidateObject(user, context, true);
            act.Should().Throw<ValidationException>().WithMessage(UsersModelResources.PasswordMinLen);
        }

        [Theory]
        [AutoNSubstituteData]
        public void UserModelSuccessfulMapsToUser(UserModel user)
        {
            //Setup mapper
            var config = new AutoMapper.MapperConfiguration(cfg => {
                cfg.AddProfile<GoC.AppStatus.WebClient.AutoMapperProfile>();
            });
            var mapper = config.CreateMapper();

            User sut = mapper.Map<User>(user);

            //Check if values were mapped correctly
            sut.ActualName.Should().Be(user.ActualName);
            sut.Email.Should().Be(user.Email);
            sut.Password.Should().Be(user.Password);
        }
    }
}