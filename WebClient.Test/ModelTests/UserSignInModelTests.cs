﻿using AutoMapper;
using FluentAssertions;
using GoC.AppStatus.DBAccess.DataAccess;
using GoC.AppStatus.DBAccess.DBEntities;
using GoC.AppStatus.WebClient.Models;
using GoC.AppStatus.WebClient.Resources;
using System;
using System.ComponentModel.DataAnnotations;
using Xunit;

namespace GoC.AppStatus.WebClient.Test.ModelTests
{
    public class UserSignInModelTests
    {
        [Theory, AutoNSubstituteData]
        public void DefaultUserWillFailAuthentication(UserSignInModel sut, IUserQueries queries, IMapper mapper)
        {
            var result = sut.Authenticate(queries, mapper);
            result.Should().BeNull();
        }

        [Theory, AutoNSubstituteData]
        public void ViewDefaultIsValid(UserSignInModel sut)
        {
            var context = new System.ComponentModel.DataAnnotations.ValidationContext(sut);
            Action act = () => Validator.ValidateObject(sut, context, true);
            act.Should().NotThrow<Exception>();
        }

        [Theory, AutoNSubstituteData]
        public void EmptyEmailShouldThrowError(UserSignInModel sut)
        {
            sut.SignInEmail = string.Empty;

            var context = new System.ComponentModel.DataAnnotations.ValidationContext(sut);
            Action act = () => Validator.ValidateObject(sut, context, true);
            act.Should().Throw<ValidationException>().WithMessage(UsersModelResources.RequiredField);
        }

        [Theory, AutoNSubstituteData]
        public void EmptyPasswordShouldThrowError(UserSignInModel sut)
        {
            sut.SignInPassword = string.Empty;

            var context = new System.ComponentModel.DataAnnotations.ValidationContext(sut);
            Action act = () => Validator.ValidateObject(sut, context, true);
            act.Should().Throw<ValidationException>().WithMessage(UsersModelResources.RequiredField);
        }


        [Theory, AutoNSubstituteData]
        public void UserSignInModelMapsToUser(UserSignInModel model, AutoMapper.IMapper mapper)
        {
            var sut = mapper.Map<User>(model);

            sut.Email.Should().Be(model.SignInEmail);
            sut.Password.Should().Be(model.SignInPassword);
        }
    }
}
