﻿using FluentAssertions;
using GoC.AppStatus.WebClient.Models;
using GoC.AppStatus.WebClient.Test;
using Xunit;
using AutoMapper;
using GoC.AppStatus.DBAccess.DBEntities;
using ValidationContext = System.ComponentModel.DataAnnotations.ValidationContext;
using System;
using System.ComponentModel.DataAnnotations;
using GoC.AppStatus.WebClient.Resources;

namespace WebClient.Test.ModelTests
{
    public class ServicesModelTests
    {
        [Theory]
        [AutoNSubstituteData]
        public void ServicesModelSuccessfulMapsToService(ServiceModel service, IMapper mapper)
        {
            //Arrange is done using the injections
            
            //Act
            Service sut = mapper.Map<Service>(service);

            //Validate
            sut.Url.Should().Be(service.Url);
            sut.Name.Should().Be(service.Name);
        }

        [Theory]
        [AutoNSubstituteData]
        public void InvalidUrlReturnsPartialViewInvalidModelState(ServiceModel service)
        {
            //Arrange: use invalid url to force modelstate.isvalid = false and returns partial view _invalidmodelstate.cshtml
            service.Url = "hello";

            //Act
            ValidationContext context = new ValidationContext(service);

            //Validate
            Action act = () => Validator.ValidateObject(service, context, true);
            act.Should().Throw<Exception>().WithMessage(ServicesModelResources.InvalidUrl);
        }



    }
}
