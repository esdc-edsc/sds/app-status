﻿using GoC.AppStatus.DBAccess.DBEntities;

namespace GoC.AppStatus.DBAccess.DataAccess
{
    public interface IUserQueries : IQueries
    {
        uint AddUser(User user);
        User GetUserByEmail(string email);
        bool IsUserEmailUsed(string email);
    }
}
