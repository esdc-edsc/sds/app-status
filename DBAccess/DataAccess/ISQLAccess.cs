﻿using GoC.AppStatus.DBAccess.DBEntities;
using System.Collections.Generic;

namespace GoC.AppStatus.DBAccess.DataAccess
{
    public interface ISQLAccess
    {
        void ConnectDatabase(string connString);
        bool DatabaseHasConnection();
        int Delete(string table, string where, int limit = 1);
        int ExecuteNonQuery(string query);
        List<IDictionary<string, string>> ExecuteReader(string query);
        int InsertRow(string table, string[] colArray, string[] valArray);
        List<IDictionary<string, string>> Select(string table, string[] colArray, string where, int limit = 1);
        List<IDictionary<string, string>> SelectAll(string table, string where = null);
        List<string> SelectAllRowsOneCol(string table, string col, string where = null);
        IDictionary<string, string> SelectMostRecent(string table, string[] colArray, string where, string colDatetimeName);
        IDictionary<string, string> SelectRow(string table, string[] colArray, string where);
        string SelectSimple(string table, string col, string where);
        string SelectSimpleMostRecent(string table, string col, string where, string colDatetimeName);
        int Update(string table, string col, string val, string where, int limit = 1);
        int Update(string table, string[] colArray, string[] valArray, string where, int limit = 1);
        int UpdateSimple(string table, string col, string val, string where, int limit = 1);
    }
}
