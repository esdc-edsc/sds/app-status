﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace GoC.AppStatus.DBAccess.DataAccess
{
    public class SQLAccess : ISQLAccess
    {
        protected readonly ILogger Logger;
        private SqlConnection _db;

        public SQLAccess(ILogger<SQLAccess> logger)
        {
            Logger = logger;
        }

        /// <summary>
        /// Establish a connection with the database.
        /// </summary>
        /// <param name="connString">The connection string to the database.</param>
        public void ConnectDatabase(string connString)
        {
            SqlConnection connection = new SqlConnection(connString);
            if (connection.State != ConnectionState.Open) connection.Open();
            if (connection.State == ConnectionState.Open) _db = connection;
            else _db = null;
        }

        /// <summary>
        /// Checks if an existing connection to the database has already been established.
        /// </summary>
        public bool DatabaseHasConnection() => _db != null && _db.State == ConnectionState.Open;

        /// <summary>
        /// Delete one or many rows.
        /// </summary>
        /// <param name="table">Table name.</param>
        /// <param name="where">Conditions.</param>
        /// <param name="limit">Max number rows to delete (default: 1).</param>
        /// <returns>Number or rows deleted; 0 in case of error.</returns>
        public int Delete(string table, string where, int limit = 1)
        {
            try
            {
                string query = "DELETE ";
                if (limit > 0) query += "TOP(" + limit + ") ";
                query += "FROM [" + table + "] @WHERE";
                where = Where(where);
                query = query.Replace("@WHERE", where);
                SqlCommand command = new SqlCommand(query, _db);

                return command.ExecuteNonQuery();
            }
            catch //(Exception e)
            {
                return 0;
            }
        }

        /// <summary>
        /// Do an ExecuteNonQuery command, and returns the result.
        /// </summary>
        /// <param name="query">A SQL query.</param>
        /// <returns>Result of the command; 0 in case of error.</returns>
        public int ExecuteNonQuery(string query)
        {
            try
            {
                SqlCommand command = new SqlCommand(query, _db);
                return command.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                string msg = "";
                for (int i = 0; i < ex.Errors.Count; i++)
                {
                    msg = "Index #" + i + "\n" +
                          "Message: " + ex.Errors[i].Message + "\n" +
                          "LineNumber: " + ex.Errors[i].LineNumber + "\n" +
                          "Source: " + ex.Errors[i].Source + "\n" +
                          "Procedure: " + ex.Errors[i].Procedure + "\n";
                }
                Console.WriteLine(msg);

                return 0;
            }
        }

        /// <summary>
        /// Do an ExecuteReader command, and returns the result as a list
        /// of "associative" array (row[colName] = colValue).
        /// </summary>
        /// <param name="query">A SQL query.</param>
        /// <returns>Result of the Select; null if there is no result found.</returns>
        public List<IDictionary<string, string>> ExecuteReader(string query)
        {
            try
            {
                List<IDictionary<string, string>> result = new List<IDictionary<string, string>>();
                SqlCommand command = new SqlCommand(query, _db);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    int count = reader.FieldCount;
                    while (reader.Read())
                    {
                        // copy the values into an "associative" array (row[colName] = colValue)
                        // as strings, and put the array in a list:
                        IDictionary<string, string> row = new Dictionary<string, string>();
                        for (int i = 0; i < count; i++)
                        {
                            row[reader.GetName(i)] = reader.GetValue(i).ToString();
                        }
                        if (row.Count > 0) result.Add(row);
                    }
                }
                reader.Close();
                if (result.Count == 0) return null; else return result;
            }
            catch (SqlException ex)
            {
                string msg = "";
                for (int i = 0; i < ex.Errors.Count; i++)
                {
                    msg = "Index #" + i + "\n" +
                          "Message: " + ex.Errors[i].Message + "\n" +
                          "LineNumber: " + ex.Errors[i].LineNumber + "\n" +
                          "Source: " + ex.Errors[i].Source + "\n" +
                          "Procedure: " + ex.Errors[i].Procedure + "\n";
                }
                Console.WriteLine(msg);

                return null;
            }
        }

        /// <summary>
        /// Insert a row in the table.
        /// </summary>
        /// <param name="table">Table name.</param>
        /// <param name="colArray">Names of the columns.</param>
        /// <param name="valArray">Values of the columns.</param>
        /// <returns>The Id of the inserted row; 0 if insertion fails.</returns>
        public int InsertRow(string table, string[] colArray, string[] valArray)
        {
            try
            {
                string query = "INSERT INTO [" + table + "]";
                string cols = "(";
                string vals = "(";
                int i = 0;
                foreach (string col in colArray)
                {
                    cols += "[" + col + "]";
                    vals += "@" + col;
                    if (i < colArray.Length - 1)
                    {
                        cols += ",";
                        vals += ",";
                    }
                    i++;
                }
                cols += ")";
                vals += ")";
                query += " " + cols + " OUTPUT INSERTED.Id " + " VALUES " + vals + ";";
                SqlCommand command = new SqlCommand(query, _db);
                i = 0;
                foreach (string col in colArray)
                    command.Parameters.AddWithValue("@" + col, valArray[i++]);

                return (int)command.ExecuteScalar();
            }
            catch (SqlException ex)
            {
                string msg = "";
                for (int i = 0; i < ex.Errors.Count; i++)
                {
                    msg = "Index #" + i + "\n" +
                          "Message: " + ex.Errors[i].Message + "\n" +
                          "LineNumber: " + ex.Errors[i].LineNumber + "\n" +
                          "Source: " + ex.Errors[i].Source + "\n" +
                          "Procedure: " + ex.Errors[i].Procedure + "\n";
                }
                Console.WriteLine(msg);

                return 0;
            }
        }

        /// <summary>
        /// Do a Select an returns the result as a list of "associative" array
        /// (row[colName] = colValue).
        /// </summary>
        /// <param name="table">Table name.</param>
        /// <param name="colArray">Columns to return (if null, all columns are returned).</param>
        /// <param name="where">Conditions.</param>
        /// <param name="limit">Max number rows to return (default: 1); if < 0, returns all rows.</param>
        /// <returns>Result of the Select; null if there is no result found.</returns>
        /// <example>
        /// Get "username", "email", "language" for the first user found
        /// in the table "users": 
        ///     Select("User", new string[] { "username", "email", "language" }, null, 1);
        /// 
        /// Get all columns for all users whom id is different from 13
        /// and lamguage is "fr":
        ///     Select("User", null, "id<>13 AND language='fr'", 0);
        /// 
        /// Get all columns for all users:
        ///     Select("User", null, null, 0);
        /// 
        /// Get the id of the user with email "toto@toto.com" (an empty
        /// list if there is none):
        ///     Select("User", new string[] { "id" }, "email='toto@toto.com'", 1);
        /// </example>
        public List<IDictionary<string, string>> Select(string table, string[] colArray, string where, int limit = 1)
        {
            try
            {
                List<IDictionary<string, string>> result = new List<IDictionary<string, string>>();
                string query = "SELECT ";
                if (limit > 0) query += "TOP(" + limit + ") ";
                query += "@WHAT FROM [" + table + "] @WHERE";
                string what = "";
                if (colArray != null && colArray.Length > 0)
                {
                    int i = 0;
                    foreach (string col in colArray)
                    {
                        what += "[" + col + "]";
                        if (i < colArray.Length - 1) what += ",";
                        i++;
                    }
                }
                else what = "*";
                query = query.Replace("@WHAT", what);
                where = Where(where);
                query = query.Replace("@WHERE", where);
                SqlCommand command = new SqlCommand(query, _db);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    if (what == "*")
                    {
                        // retrieve all columns names:
                        var columns = new List<string>();
                        for (int i = 0; i < reader.FieldCount; i++)
                            columns.Add(reader.GetName(i));
                        colArray = columns.ToArray();
                    }
                    while (reader.Read())
                    {
                        // copy the values into an "associative" array (row[colName] = colValue)
                        // as strings, and put the array in a list:
                        IDictionary<string, string> row = new Dictionary<string, string>();
                        foreach (string col in colArray)
                        {
                            int index = reader.GetOrdinal(col);
                            if (!reader.IsDBNull(index))
                                row[col] = reader.GetValue(index).ToString();
                            else row[col] = "";
                        }
                        if (row.Count > 0) result.Add(row);
                    }
                }
                reader.Close();
                if (result.Count == 0) return null; else return result;
            }
            catch (SqlException ex)
            {
                string msg = "";
                for (int i = 0; i < ex.Errors.Count; i++)
                {
                    msg = "Index #" + i + "\n" +
                          "Message: " + ex.Errors[i].Message + "\n" +
                          "LineNumber: " + ex.Errors[i].LineNumber + "\n" +
                          "Source: " + ex.Errors[i].Source + "\n" +
                          "Procedure: " + ex.Errors[i].Procedure + "\n";
                }
                Console.WriteLine(msg);

                return null;
            }
        }

        /// <summary>
        /// Select all the table verifying the conditions.
        /// </summary>
        /// <param name="table">Table name.</param>
        /// <param name="where">Conditions.</param>
        /// <returns>All the table verifying the conditions; null if there is no result found.</returns>
        public List<IDictionary<string, string>> SelectAll(string table, string where = null)
        {
            return Select(table, null, where, 0);
        }

        /// <summary>
        /// Returns one col of all rows from the table.
        /// </summary>
        /// <param name="table">Table name.</param>
        /// <param name="col">Column to return.</param>
        /// <param name="where">Conditions.</param>
        /// <returns>One col of all rows from the table; null if there is no result found.</returns>
        public List<string> SelectAllRowsOneCol(string table, string col, string where = null)
        {
            if (col == null) return null;

            List<IDictionary<string, string>> rows = Select(table, new string[] { col }, where, 0);
            if (rows == null) return null;

            List<string> list = new List<string>();
            foreach (IDictionary<string, string> row in rows)
            {
                list.Add(row[col]);
            }

            return list;
        }

        /// <summary>
        /// Returns the most recent row in the table.
        /// </summary>
        /// <param name="table">Table name.</param>
        /// <param name="colArray">Columns to return (if null, all columns are returned).</param>
        /// <param name="where">Conditions.</param>
        /// <param name="colDatetimeName">Name of the column given the date.</param>
        /// <returns>Most recent row in the table.</returns>
        public IDictionary<string, string> SelectMostRecent(string table, string[] colArray, string where, string colDatetimeName)
        {
            where = Where(where, colDatetimeName);
            List<IDictionary<string, string>> rows = Select(table, colArray, where, 1);
            if (rows == null) return null; else return rows.First();
        }

        /// <summary>
        /// Returns one row from the table.
        /// </summary>
        /// <param name="table">Table name.</param>
        /// <param name="colArray">Columns to return (if null, all columns are returned).</param>
        /// <param name="where">Conditions.</param>
        /// <returns>One row from the table.</returns>
        public IDictionary<string, string> SelectRow(string table, string[] colArray, string where)
        {
            List<IDictionary<string, string>> rows = Select(table, colArray, where, 1);
            if (rows == null) return null; else return rows.First();
        }

        /// <summary>
        /// Do a simple Select and returns the result as a string.
        /// </summary>
        /// <param name="table">Table name.</param>
        /// <param name="col">Column to return.</param>
        /// <param name="where">Conditions.</param>
        /// <returns>The result as a string; null if there is no result found.</returns>
        /// <example>
        /// Get the id of the user with email "toto@toto.com" (an empty
        /// string if there is none):
        ///     SelectSimple("User", "id", "email='toto@toto.com'");
        /// Note we get the same result (but as a list), with line:
        ///     Select("User", new string[] { "id" }, "email='toto@toto.com'", 1);
        /// </example>
        public string SelectSimple(string table, string col, string where)
        {
            List<IDictionary<string, string>> result = Select(table, new string[] { col }, where, 1);
            if (result != null && result.Count == 1) return result[0][col]; else return null;
        }

        /// <summary>
        /// Returns the most recent result.
        /// </summary>
        /// <param name="table">Table name.</param>
        /// <param name="col">Column to return.</param>
        /// <param name="where">Conditions.</param>
        /// <param name="colDatetimeName">Name of the column given the date.</param>
        /// <returns>Most recent result.</returns>
        public string SelectSimpleMostRecent(string table, string col, string where, string colDatetimeName)
        {
            where = Where(where, colDatetimeName);
            return SelectSimple(table, col, where);
        }

        /// <summary>
        /// Update one or many rows.
        /// </summary>
        /// <param name="table">Table name.</param>
        /// <param name="col">Column to update.</param>
        /// <param name="val">New value for column.</param>
        /// <param name="where">Conditions.</param>
        /// <param name="limit">Max number rows to update (default: 1).</param>
        /// <returns>Number of rows updates, or 0 in case of error.</returns>
        public int Update(string table, string col, string val, string where, int limit = 1)
        {
            return Update(table, new string[] { col }, new string[] { val }, where, limit);
        }

        /// <summary>
        /// Update one or many rows.
        /// </summary>
        /// <param name="table">Table name.</param>
        /// <param name="colArray">Columns to update.</param>
        /// <param name="valArray">New values for each columns (in same order).</param>
        /// <param name="where">Conditions.</param>
        /// <param name="limit">Max number rows to update (default: 1).</param>
        /// <returns>Number of rows updates, or 0 in case of error.</returns>
        /// <example>
        /// Inside table "user", for the first line found where username='paul.dupond',
        /// change column "language" to "fr" and column "tmp" to "2222":
        ///     Update("user", new string[] { "language", "tmp" }, new string[] { "fr", "2222" }, "[username]='paul.dupond'");
        /// 
        /// Inside table "user", change column "language" to "fr" for all rows:
        ///     Update("user", new string[] { "language" }, new string[] { "fr" }, null, 0);
        /// </example>
        public int Update(string table, string[] colArray, string[] valArray, string where, int limit = 1)
        {
            try
            {
                string query = "UPDATE ";
                if (limit > 0) query += "TOP(" + limit + ") ";
                query += "[" + table + "] SET @WHAT @WHERE";
                string what = "";
                if (colArray != null && colArray.Length > 0)
                {
                    int i = 0;
                    foreach (string col in colArray)
                    {
                        what += "[" + col + "] = '" + valArray[i] + "' ";
                        if (i < colArray.Length - 1) what += ", ";
                        i++;
                    }
                }
                else what = "*";
                query = query.Replace("@WHAT", what);
                where = Where(where);
                query = query.Replace("@WHERE", where);
                SqlCommand command = new SqlCommand(query, _db);

                return command.ExecuteNonQuery();
            }
            catch //(Exception e)
            {
                return 0;
            }
        }

        /// <summary>
        /// Update one column from one or many rows.
        /// </summary>
        /// <param name="table">Table name.</param>
        /// <param name="col">Column to update.</param>
        /// <param name="val">New value.</param>
        /// <param name="where">Conditions.</param>
        /// <param name="limit">Max number rows to update (default: 1).</param>
        /// <returns>Number of rows updates, or 0 in case of error.</returns>
        /// <example>
        /// Inside table "user", change column "language" to "fr" for all rows:
        ///     UpdateSimple("user", "language", "fr", null, 0);
        /// We get the same result with:
        ///     Update("user", new string[] { "language" }, new string[] { "fr" }, null, 0);
        /// </example>
        public int UpdateSimple(string table, string col, string val, string where, int limit = 1)
        {
            return Update(table, new string[] { col }, new string[] { val }, where, limit);
        }

        /// <summary>
        /// Cleans a "WHERE" clause and eventually add a "ORDER BY DESC".
        /// </summary>
        /// <param name="where">Clause.</param>
        /// <param name="colOrderName">Name of column to use for the order.</param>
        /// <returns>Cleaned clause.</returns>
        /// <example>
        /// string s1 = "[app] = 'tata' && [username] = 'paul'";
        /// string s2 = Sql.Where(s1, "insert_datetime"); // "WHERE ([app] = 'tata' && [username] = 'paul') ORDER BY [insert_datetime] DESC"
        /// string s3 = Sql.Where(s2); // "WHERE ([app] = 'tata' && [username] = 'paul') ORDER BY [insert_datetime] DESC"
        /// string s4 = Sql.Where(s1); // "WHERE ([app] = 'tata' && [username] = 'paul')"
        /// string s5 = "";
        /// string s6 = Sql.Where(s5, "insert_datetime"); // "ORDER BY [insert_datetime] DESC"
        /// string s7 = Sql.Where(s6); // "ORDER BY [insert_datetime] DESC"
        /// string s8 = Sql.Where(s5); // ""
        /// string s9 = Sql.Where(s5, ""); // ""
        /// string s10 = Sql.Where(s9); // ""
        /// string s11 = Sql.Where(s1); // "WHERE ([app] = 'tata' && [username] = 'paul')"
        /// string s12 = Sql.Where(s11, "insertion_datetime"); // "WHERE ([app] = 'tata' && [username] = 'paul') ORDER BY [insert_datetime] DESC"
        /// </example>
        private string Where(string where, string colOrderName = null)
        {
            // clean clause:
            if (where == null) where = "";
            else where = where.Trim();
            if (colOrderName == null) colOrderName = "";
            else colOrderName = colOrderName.Trim();

            // extract where and order subclauses:
            bool hasWhere = false;
            bool hasOrder = false;
            string order = "";
            if (colOrderName != "")
            {
                // clause is supposed to NOT contain "ORDER" subclause:
                // nothing special, because this case should not occur...
            }
            else
            {
                // clause MAY contain "ORDER" subclause:
                // get where part (text before "ORDER"):
                int pos = where.IndexOf("ORDER");
                if (pos != -1)
                {
                    // clause contains "ORDER":
                    hasOrder = true;
                    order = where.Substring(pos + 5).Trim();
                    where = where.Substring(0, pos).Trim();
                }
            }
            // clean where subclause:
            string[] result = where.Split("WHERE", StringSplitOptions.RemoveEmptyEntries);
            if (result.Length != 1)
            {
                // nothing special, because this case should not occur...
            }
            else
            {
                where = result[0].Trim();
                if (where != "") hasWhere = true;
            }

            //where = where.Trim(new char[] { '(', ')' }); // DON'T UNCOMMENT !!
            where = where.Trim();

            // rebuild where + order clause:
            if (hasWhere) where = "WHERE (" + where + ") "; else where = "";
            //if (hasWhere) where = "WHERE " + where + " "; else where = "";
            if (colOrderName != "")
                where += "ORDER BY [" + colOrderName + "] DESC";
            else if (hasOrder) where += "ORDER " + order;
            where = where.Trim();

            return where;
        }

    }
}
