﻿using GoC.AppStatus.DBAccess.DBEntities;
using System;
using System.Collections.Generic;

namespace GoC.AppStatus.DBAccess.DataAccess
{
    public interface IEnvironmentQueries : IQueries
    {
        void AddEnvironment(DBEntities.Environment user);
        bool EnvironmentUsedByAtLeastOneService(uint environmentId);
        IEnumerable<DBEntities.Environment> GetAllEnvironments(uint userId = 0);
        DBEntities.Environment GetEnvironmentById(uint environmentId);
        bool IsEnvTitlelUsed(string envTitle, uint userId);
        bool RemoveEnvironment(uint environmentId);
        bool UpdateEnvironment(uint environmentId, DBEntities.Environment environment);
    }
}
