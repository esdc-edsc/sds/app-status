﻿using GoC.AppStatus.DBAccess.DBEntities;
using System.Collections.Generic;

namespace GoC.AppStatus.DBAccess.DataAccess
{
    public interface ISubscriptionQueries : IQueries
    {
        bool AddSubscription(Subscription subscription);
        IEnumerable<User> GetSubscribedUsers(Service service);
        bool RemoveSubscription(uint serviceId);
        uint GetSubscriptionIdFromServiceId(uint ServiceId);
        bool SetSubscriptionStatus(uint id, bool subscribe);
        bool Subscribe(uint id);
        bool Unsubscribe(uint id);
        
    }
}
