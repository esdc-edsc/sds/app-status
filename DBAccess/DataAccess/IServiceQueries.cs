﻿using GoC.AppStatus.DBAccess.DBEntities;
using System;
using System.Collections.Generic;

namespace GoC.AppStatus.DBAccess.DataAccess
{
    public interface IServiceQueries : IQueries
    {
        void AddService(Service service);
        bool IsServiceNameUsed(string name, uint userId);
        bool IsServiceUrlUsed(string url, uint userId);
        IEnumerable<Service> GetAllServices(uint userId = 0);
        IEnumerable<ServiceView> GetAllServicesView(uint userId = 0);
        void ReportFailedService(ServiceFailureReport serviceFailure);
        ServiceFailureReport GetActiveFailureReport(uint serviceId);
        bool ReportFailedServiceResolved(ServiceFailureReport resolvedServiceFailure);
        Service GetServiceById(uint Id);
        bool UpdateService(uint serviceId, Service service);
        bool RemoveService(uint serviceId);
    }
}
