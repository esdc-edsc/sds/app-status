﻿using GoC.AppStatus.DBAccess.DBEntities;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace GoC.AppStatus.DBAccess.DataAccess
{
    public class Queries : SQLAccess, IUserQueries, IServiceQueries, ISubscriptionQueries, IEnvironmentQueries
    {
        public Queries(ILogger<Queries> logger) : base(logger) {}

        /// <summary>
        /// Creates a object of type T filled with the properties
        /// (key,value) contained in the SQL result.
        /// </summary>
        /// <typeparam name="T">Type of Object to return.</typeparam>
        /// <param name="sqlResult">Result of a SQL request.</param>
        /// <returns>A filled object of type T.</returns>
        private T ConvertSqlResultToObject<T>(IDictionary<string, string> sqlResult) where T : new()
        {
            T o = new T();
            if (sqlResult == null) return o;

            foreach (KeyValuePair<string, string> entry in sqlResult)
            {
                // retrieve type of object property (if the property exists):
                var prop = o.GetType().GetProperty(entry.Key);
                if (prop != null)
                {
                    // convert value to the type of the property:
                    var converter = TypeDescriptor.GetConverter(prop.PropertyType);

                    if (entry.Value != "")
                    {
                        var value = converter.ConvertFromString(entry.Value);
                        // set the property value:
                        o.GetType().GetProperty(entry.Key).SetValue(o, value);
                    }
                    else o.GetType().GetProperty(entry.Key).SetValue(o, null);
                }
            }

            return o;
        }

        /// <summary>
        /// Creates a list of objects of type T filled with the
        /// list of properties (key,value) contained in the SQL result.
        /// </summary>
        /// <typeparam name="T">Type of Objects to return.</typeparam>
        /// <param name="sqlResult">Result of a SQL request.</param>
        /// <returns>A list of filled objects of type T.</returns>
        private IEnumerable<T> ConvertSqlResultToListObjects<T>(List<IDictionary<string, string>> sqlResults) where T : new()
        {
            List<T> list = new List<T>();
            if (sqlResults == null) return list;

            foreach (IDictionary<string, string> sqlResult in sqlResults)
            {
                list.Add(ConvertSqlResultToObject<T>(sqlResult));
            }

            return list;
        }

        private string GetTableName<T>()
        {
            return typeof(T).Name + 's';
        }

        /// <summary>
        /// Returns all the objects of a type of DBEntities.
        /// </summary>
        /// <typeparam name="T">Type of Objects to return.</typeparam>
        /// <returns>All the objects of a type of DBEntities; empty list if none.</returns>
        private IEnumerable<T> GetAll<T>() where T : new()
        {
            return ConvertSqlResultToListObjects<T>(SelectAll(GetTableName<T>()));
        }

        #region UserQueries
        public uint AddUser(User user)
        {
            if (user == null) throw new ArgumentNullException(nameof(user));

            string[] col = { "ActualName", "Email", "Password"};
            string[] val = { user.ActualName, user.Email, user.Password };
            return (uint)InsertRow(GetTableName<User>(), col, val);
        }

        public User GetUserByEmail(string email)
        {
            if (String.IsNullOrEmpty(email)) throw new ArgumentNullException(nameof(email));

            IDictionary<string, string> result = SelectRow(GetTableName<User>(), null, "[Email]='" + email + "'");
            if (result == null) return null;
            
            return ConvertSqlResultToObject<User>(result);
        }

        public bool IsUserEmailUsed(string email)
        {
            if (String.IsNullOrEmpty(email)) throw new ArgumentNullException(nameof(email));

            return SelectSimple(GetTableName<User>(), "Email", "[Email]='" + email + "'") != null;
        }
        #endregion UserQueries

        #region ServiceQueries
        public void AddService(Service service)
        {
            if (service == null) throw new ArgumentNullException(nameof(service));

            List<string> col = new List<string> { "Url", "Name", "CreatedByUserId" };
            List<string> val = new List<string> { service.Url, service.Name, service.CreatedByUserId.ToString() };

            if (service.EnvironmentId != 0)
            {
                col.Add("EnvironmentId");
                val.Add(service.EnvironmentId.ToString());
            }
            InsertRow(GetTableName<Service>(), col.ToArray(), val.ToArray());
        }

        public bool IsServiceNameUsed(string name, uint userId)
        {
            string where = "[Name]='" + name + "' AND [DisabledDatetime] IS NULL AND [CreatedByUserId]=" + userId;
            return SelectSimple(GetTableName<Service>(), "Id", where) != null;
        }

        public bool IsServiceUrlUsed(string url, uint userId)
        {
            // log example; requires in appsettings.json: ""LogLevel": {"Default": "Trace"}"
            // Logger.LogTrace("[Queries.cs - IsServiceUrlUsed] LogTrace...");

            string where = "[Url]='" + url + "' AND [DisabledDatetime] IS NULL AND [CreatedByUserId]=" + userId;
            return SelectSimple(GetTableName<Service>(), "Id", where) != null;
        }

        /// <summary>
        /// If userId != 0, get services created by the user;
        /// Otherwise, get all services.
        /// </summary>
        /// <param name="userId">User Id.</param>
        /// <returns>Services created by the user, or all services.</returns>
        public IEnumerable<Service> GetAllServices(uint userId = 0)
        {
            var subscribedServiceIds = GetSubscribedServiceIds();

            // get all services created by the user:
            string where = "";
            if (userId > 0) where = "[CreatedByUserId]=" + userId;
            IEnumerable<Service> services = ConvertSqlResultToListObjects<Service>(SelectAll(GetTableName<Service>(), where));

            // set up "Subscribed" property for each service:
            List<Service> servicesOut = new List<Service>();
            foreach (Service service in services)
            {
                service.Subscribed = false;
                if (subscribedServiceIds != null)
                {
                    foreach (uint subscribedServiceId in subscribedServiceIds)
                    {
                        if (subscribedServiceId == service.Id)
                        {
                            service.Subscribed = true;
                            break;
                        }
                    }
                }
                servicesOut.Add(service);
            }

            return servicesOut;
        }

        /// <summary>
        /// If userId != 0, get services created by the user, with
        /// all data required by the View Dashboard; Otherwise, get all services.
        /// </summary>
        /// <param name="userId">User Id.</param>
        /// <returns>Services created by the user, or all services.</returns>
        public IEnumerable<ServiceView> GetAllServicesView(uint userId = 0)
        {
            // get all services created by the user:
            string tableServices = GetTableName<Service>();
            string tableEnvironments = GetTableName<DBEntities.Environment>();
            string query = @"SELECT " +
                "[" + tableServices + "].*, " +
                "[" + tableEnvironments + "].[Id] AS envId, " +
                "[" + tableEnvironments + "].[Colour] AS envColour, " +
                "[" + tableEnvironments + "].[Title] AS envTitle " +
                "FROM [" + tableServices + "] " +
                "LEFT JOIN [" + tableEnvironments + "] ON [" + tableServices + "].[EnvironmentId] = [" + tableEnvironments + "].[Id]";
            if (userId > 0) query += " WHERE [" + tableServices + "].[CreatedByUserId] = " + userId;
            IEnumerable<ServiceView> services = ConvertSqlResultToListObjects<ServiceView>(ExecuteReader(query));

            // refresh and store in DB prop "CertExpiryDays" for all secured services,
            // only when it's needed (once time a day):
            foreach (ServiceView service in services)
            {
                if (service.Url.StartsWith("https", StringComparison.InvariantCultureIgnoreCase))
                {
                    if (service.CertExpiryDaysDatetime.Date != DateTime.Today)
                    {
                        int n = GetUrlCertificateExpiryDays(new Uri(service.Url));
                        service.CertExpiryDays = n;
                        service.CertExpiryDaysDatetime = DateTime.Now;
                        string[] col = { "CertExpiryDays", "CertExpiryDaysDatetime" };
                        string[] val = { n.ToString(), DateTimeToString(service.CertExpiryDaysDatetime) };
                        Update(GetTableName<Service>(), col, val, "[Id]=" + service.Id);
                    }
                }
            }

            // get Ids of all subscribed services:
            var subscribedServiceIds = GetSubscribedServiceIds();

            // set up "Subscribed" property for each service:
            List<ServiceView> servicesOut = new List<ServiceView>();
            foreach (ServiceView service in services)
            {
                service.Subscribed = false;
                if (subscribedServiceIds != null)
                {
                    foreach (uint subscribedServiceId in subscribedServiceIds)
                    {
                        if (subscribedServiceId == service.Id)
                        {
                            service.Subscribed = true;
                            break;
                        }
                    }
                }
                servicesOut.Add(service);
            }

            return servicesOut;
        }

        /// <summary>
        /// Return Ids of subscribed services.
        /// </summary>
        /// <returns>Ids of subscribed services; empty list if there is no subscribed service.</returns>
        private List<uint> GetSubscribedServiceIds()
        {
            List<string> lStr = SelectAllRowsOneCol(GetTableName<Subscription>(), "ServiceId", "SubscribedDatetime IS NOT NULL");
            if (lStr == null) return null;
            List<uint> lUint = new List<uint>();
            foreach (string s in lStr) lUint.Add(Convert.ToUInt32(s, 10));

            return lUint;
        }

        public void ReportFailedService(ServiceFailureReport serviceFailure)
        {
            if (serviceFailure == null) throw new ArgumentNullException(nameof(serviceFailure));

            string[] col = { "ServiceId", "FaildWith", "FirstReported" };
            string[] val = { serviceFailure.ServiceId.ToString(), ((int)serviceFailure.FaildWith).ToString(), DateTimeToString() };
            InsertRow(GetTableName<ServiceFailureReport>(), col, val);
        }

        public ServiceFailureReport GetActiveFailureReport(uint serviceId)
        {
            if (serviceId <= 0) throw new ArgumentException(nameof(serviceId));

            string where = "[ServiceId]=" + serviceId + " AND [Resolved] IS NULL";
            IDictionary<string, string> result = SelectRow(GetTableName<ServiceFailureReport>(), null, where);
            if (result == null) return null;

            return ConvertSqlResultToObject<ServiceFailureReport>(result);
        }

        public bool ReportFailedServiceResolved(ServiceFailureReport resolvedServiceFailure)
        {
            if (resolvedServiceFailure == null) throw new ArgumentNullException(nameof(resolvedServiceFailure));

            string[] col = { "Resolved" };
            string[] val = { DateTimeToString() };
            return Update(GetTableName<ServiceFailureReport>(), col, val, "[Id]=" + resolvedServiceFailure.Id) != 0;
        }

        public Service GetServiceById(uint serviceId)
        {
            if (serviceId <= 0) throw new ArgumentException(nameof(serviceId));

            IDictionary<string, string> result = SelectRow(GetTableName<Service>(), null, "[Id]=" + serviceId);
            if (result == null) return null;

            return ConvertSqlResultToObject<Service>(result);
        }

        public bool UpdateService(uint serviceId, Service service)
        {
            if (serviceId <= 0) throw new ArgumentException(nameof(serviceId));

            string[] col = { "Url", "Name", "EnvironmentId" };
            string[] val = { service.Url, service.Name, service.EnvironmentId.ToString() };
            return Update(GetTableName<Service>(), col, val, "[Id]=" + serviceId) != 0;
        }

        public bool RemoveService(uint serviceId)
        {
            if (serviceId <= 0) throw new ArgumentException(nameof(serviceId));

            // if subscription exists, remove it:
            var subscriptionId = GetSubscriptionIdFromServiceId(serviceId);
            if (subscriptionId != 0 && !RemoveSubscription(serviceId))
            {
                return false;
            }

            return UpdateSimple(GetTableName<Service>(), "DisabledDatetime", DateTimeToString(), "[Id]=" + serviceId) != 0;
        }
        #endregion ServiceQueries

        #region SubscriptionQueries

        public bool AddSubscription(Subscription subscription)
        {
            if (subscription == null) throw new ArgumentNullException(nameof(subscription));

            string[] col = { "UserId", "ServiceId", "SubscribedDatetime" };
            string[] val = { subscription.UserId.ToString(), subscription.ServiceId.ToString(), DateTimeToString() };
            return InsertRow(GetTableName<Subscription>(), col, val) != 0;
        }

        public IEnumerable<User> GetSubscribedUsers(Service service)
        {
            if (service == null) throw new ArgumentNullException(nameof(service));

            string tableUsers = GetTableName<User>();
            string tableSubscriptions = GetTableName<Subscription>();
            string query = @"SELECT [" + tableUsers + "].* FROM [" + tableUsers + "]" +
                "LEFT JOIN [" + tableSubscriptions + "] ON [" + tableUsers + "].[Id] = [" + tableSubscriptions + "].[UserId] " + 
                "WHERE [" + tableSubscriptions + "].ServiceId = " + service.Id;
            List<IDictionary<string, string>> users = ExecuteReader(query);
            if (users == null) return null;

            return ConvertSqlResultToListObjects<User>(users);
        }

        public bool RemoveSubscription(uint serviceId)
        {
            if (serviceId <= 0) throw new ArgumentException(nameof(serviceId));

            return Delete(GetTableName<Subscription>(), "[ServiceId]=" + serviceId) != 0;
        }

        public uint GetSubscriptionIdFromServiceId(uint serviceId)
        {
            if (serviceId <= 0) throw new ArgumentException(nameof(serviceId));

            string subscriptionId = SelectSimple(GetTableName<Subscription>(), "Id", "[ServiceId]=" + serviceId);
            if (subscriptionId == null) return 0;
            return Convert.ToUInt32(subscriptionId);
        }

        public bool Subscribe(uint id)
        {
            return SetSubscriptionStatus(id, true);
        }

        public bool Unsubscribe(uint id)
        {
            return SetSubscriptionStatus(id, false);
        }
        public bool SetSubscriptionStatus(uint id, bool subscribe)
        {
            if (id <= 0) throw new ArgumentException(nameof(id));

            string query = "UPDATE [" + GetTableName<Subscription>() + "] SET ";
            if (subscribe) query += "[SubscribedDatetime]=GETDATE(), [UnsubscribedDatetime]=NULL ";
            else query += "[SubscribedDatetime]=NULL, [UnsubscribedDatetime]=GETDATE() ";
            query += "WHERE [id]=" + id;

            return ExecuteNonQuery(query) != 0;
        }
        #endregion SubscriptionQueries

        #region EnvironmentQueries

        public void AddEnvironment(DBEntities.Environment environment)
        {
            if (environment == null) throw new ArgumentNullException(nameof(environment));

            string[] col = { "Colour", "CreatedByUserId", "Description", "Title" };
            string[] val = { environment.Colour, environment.CreatedByUserId.ToString(), environment.Description, environment.Title };
            InsertRow(GetTableName<DBEntities.Environment>(), col, val);
        }

        public bool EnvironmentUsedByAtLeastOneService(uint environmentId)
        {
            if (environmentId <= 0) throw new ArgumentException(nameof(environmentId));

            string where = "[EnvironmentId]=" + environmentId + " AND [DisabledDatetime] IS NULL";
            return SelectSimple(GetTableName<Service>(), "Id", where)! != null;
        }

        /// <summary>
        /// If userId != 0, get environments created by the user;
        /// Otherwise, get all environments.
        /// </summary>
        /// <param name="userId">User Id.</param>
        /// <returns>Environments created by the user, or all environments.</returns>
        public IEnumerable<DBEntities.Environment> GetAllEnvironments(uint userId = 0)
        {
            string where = "";
            if (userId > 0) where = "[CreatedByUserId]=" + userId;
            return ConvertSqlResultToListObjects<DBEntities.Environment>(SelectAll(GetTableName<DBEntities.Environment>(), where));
        }

        public DBEntities.Environment GetEnvironmentById(uint environmentId)
        {
            if (environmentId <= 0) throw new ArgumentException(nameof(environmentId));

            IDictionary<string, string> result = SelectRow(GetTableName<DBEntities.Environment>(), null, "[Id]=" + environmentId);
            if (result == null) return null;

            return ConvertSqlResultToObject<DBEntities.Environment>(result);
        }

        public bool IsEnvTitlelUsed(string envTitle, uint userId)
        {
            string where = "[Title]='" + envTitle + "' AND [CreatedByUserId]=" + userId;
            return SelectSimple(GetTableName<DBEntities.Environment>(), "Id", where) != null;
        }

        public bool RemoveEnvironment(uint environmentId)
        {
            if (environmentId <= 0) throw new ArgumentException(nameof(environmentId));

            if (EnvironmentUsedByAtLeastOneService(environmentId)) return false;

            string where = "[id]=" + environmentId;
            return Delete(GetTableName<DBEntities.Environment>(), where) != 0;
        }

        public bool UpdateEnvironment(uint environmentId, DBEntities.Environment environment)
        {
            if (environmentId <= 0) throw new ArgumentException(nameof(environmentId));

            string[] col = { "Colour", "Description", "Title" };
            string[] val = { environment.Colour, environment.Description, environment.Title };
            return Update(GetTableName<DBEntities.Environment>(), col, val, "[Id]=" + environmentId) != 0;
        }
        #endregion EnvironmentQueries

        #region MiscTools
        private string DateTimeToString()
        {
            return DateTimeToString(null);
        }

        private string DateTimeToString(DateTime? datetime)
        {
            DateTime dt;
            if (datetime == null) dt = DateTime.Now; else dt = (DateTime)datetime;
            //return DateTime.Now.ToString("yyyy'-'MM'-'dd' 'HH':'mm':'ss");
            return dt.ToString("yyyy'-'MM'-'dd' 'HH':'mm':'ss");
        }

        /// <summary>
        /// Initiates a web request to the specified URL and retrieves the number of days until the TLS (HTTPS) certificate expires.
        /// </summary>
        /// <param name="checkTargetUrl">The URL for which to retrieve expiry information.</param>
        /// <returns>
        /// The numbers of days (from current time) until the URL's TLS certificate expires. 
        /// A value of &lt;= 0 indicates the certificate is already expired.
        /// If the URL's protocol is not HTTPS, int.MaxValue is returned.
        /// </returns>
        /// <remarks>
        /// Requires .NET Framework 4.7+ or .NET Core 2.1+
        /// Requires the assembly references System.Net.Http
        /// </remarks>
        public static int GetUrlCertificateExpiryDays(Uri checkTargetUrl)
        {
            if (checkTargetUrl == null) throw new NullReferenceException("A checkTargetUrl must be specified");

            if (checkTargetUrl.Scheme == Uri.UriSchemeHttps)
            {
                try
                {
                    var requestHandler = new System.Net.Http.HttpClientHandler();
                    DateTime? certExpiry = null;

                    requestHandler.ServerCertificateCustomValidationCallback += (message, cert, chain, policyErrors) =>
                    {
                        certExpiry = cert?.NotAfter;
                        //returning true blindly (ie trusting everyone automatically) is normally very bad,
                        //but for the purposes of only checking certificate info, it'll be ok.
                        return true;
                    };

                    var client = new System.Net.Http.HttpClient(requestHandler);

                    client.GetAsync(checkTargetUrl).GetAwaiter().GetResult(); //don't really care about the response contents

                    var expiryDays = certExpiry?.Subtract(DateTime.Now).Days;
                    if (expiryDays == null)
                    {
                        throw new ApplicationException("Cannot verify expiry, a certificate was unexpectedly not found in the web response.");
                    }

                    return expiryDays ?? 0;
                }
                catch
                {
                    // NOTE: Optionally log errors 
                    throw;
                }
            }

            // If we get here, URL was not HTTPS
            return int.MaxValue;
        }
        #endregion MiscTools

    }

}
