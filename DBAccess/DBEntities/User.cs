﻿namespace GoC.AppStatus.DBAccess.DBEntities
{
    //public class User : IUser
    public class User
    {
        public uint Id { get; set; }
        public string ActualName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
