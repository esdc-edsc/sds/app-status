﻿using System;
using System.Net;

namespace GoC.AppStatus.DBAccess.DBEntities
{
    public class ServiceFailureReport
    {
        public uint Id { get; set; }
        public uint ServiceId { get; set; }
        public HttpStatusCode FaildWith { get; set; }
        public DateTime FirstReported { get; set; }
        public DateTime? Resolved { get; set; }
    }
}
