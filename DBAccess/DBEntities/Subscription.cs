﻿using System;

namespace GoC.AppStatus.DBAccess.DBEntities
{
    public class Subscription
    {
        public uint Id { get; set; }
        public uint UserId { get; set; }
        public uint ServiceId { get; set; }
        public DateTime SubscribedDatetime { get; set; }
        public DateTime UnsubscribedDatetime { get; set; }
    }
}
