using System;

namespace GoC.AppStatus.DBAccess.DBEntities
{
    public class Environment
    {
        public uint Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Colour { get; set; }
        public uint CreatedByUserId { get; set; }
    }
}
