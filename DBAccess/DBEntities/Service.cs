using System;

namespace GoC.AppStatus.DBAccess.DBEntities
{
    public class Service
    {
        public uint Id { get; set; }
        public string Url { get; set; }
        public string Name { get; set; }
        public uint EnvironmentId { get; set; }
        public uint CreatedByUserId { get; set; }
        public DateTime DisabledDatetime { get; set; }
        public bool Subscribed { get; set; }
    }
}
