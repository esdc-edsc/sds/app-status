const { credentials } = require('./secrets.js')
const { AxePuppeteer } = require('axe-puppeteer');
const AxeReports = require("axe-reports");
const puppeteer = require('puppeteer');

//Required if executing script from pipeline
//var PASSWORD = process.argv[3];

async function a11ytest(page, name) {
  console.log("Assessing: " + page.url());
  const results = await new AxePuppeteer(page)
    .withTags(["wcag2a", "wcag2aa"])
    .exclude([
      "ul[role=menubar]" //Exclude MenuBar as it's WET and not accessible
    ])
    .analyze().then(function(results) {
		AxeReports.processResults(results, "csv", "reports/" + name, true);
		expect (results.violations.length).toBe(0);
	});
}

async function screenshot(page, name) {
  return page.screenshot({ path: "screenshots/" + name + ".png" });
}

async function runtest(page, name) {
  return Promise.all([a11ytest(page, name), screenshot(page, name)]);
}

describe("Puppeteer accessibility tests for AppStatus.", function() {
	beforeAll(async () => {
		const _browser = await puppeteer.launch({
			headless: true,
			ignoreHTTPSErrors: true,
			args: ["--no-sandbox", "--disable-setuid-sandbox", "--disable-dev-shm-usage"]
		});
		const _page = await _browser.newPage();
		
		browser = _browser;
		page = _page;		
	});
	
	test( 'Testing Accessibility for AppStatus', async () => {
		console.log("Navigating to AppStatus home page");
		await page.goto('https://esdcappstatus-staging.azurewebsites.net/');
		await runtest(page, "Dashboard-notsignedin");
	}, 60000);
	
	test( 'Testing the AppStatus Dashboard when signed in', async () => {
		console.log("Testing the AppStatus Dashboard when signed in");
		await page.evaluate(() => document.querySelector('a[href$="#login"').click());
		await page.type('#signInUserName', credentials.username);
		await page.type('#signInPassword', credentials.password);
		await page.evaluate(() => document.querySelector('button[type$="submit"]').click());
		await page.waitForNavigation();
		await runtest(page, "Dashboard-signedin");
	}, 60000);
	
	test( 'Testing the Register Services Page', async () => {
		console.log("Testing the Register Services page");
		await page.evaluate(() => document.querySelector('a[href$="/Services/Register"]').click());
		await page.waitForNavigation();
		await runtest(page, "RegisterServices");
	}, 60000);
	
	test( 'Testing the Register User Page', async () => {
		console.log("Testing the Register User Page");
		await page.evaluate(() => document.querySelector('a[href$="/Account/Logout"]').click());
		await page.waitForNavigation();
		await page.goto('https://esdcappstatus-staging.azurewebsites.net/Account/Register');
		await runtest(page, "RegisterUser");
	}, 60000);
	
	afterAll(async () => {
		await page.close();
		await browser.close();
	});
});
