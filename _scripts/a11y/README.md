# This guide will walk through how to run Accessibility tests for AppStatus

## Pre-requisites:

Download Node.js. 

1. Go to https://nodejs.org/en/download/current/ and download and install the latest version of Node.js.
2. Make sure to add the path where Node.js was installed at, to the PATH environment variable. 
3. To test if you have successfully installed Node.js, open cmd and type 'node -v' and you should see the version of Node.js installed.

## Setup:

1. Start cmd in the current directory

2. Run the following (one time) command that will install all the necessary libraries:

```
npm i 
```

3a. Create a file in this current directory and call it 'secrets.js'
3b. Add the following code substituting with your actual staging username and password: 

```
const credentials = {
  username  : "#######",
  password : "#######"
};
module.exports = { credentials };
```

## To run the script:

1. From cmd, run the following command: 

```
npm test
```

2. The test will run. The reports and screenshots will be saved in the 'reports' and 'screenshots' folder respectively.