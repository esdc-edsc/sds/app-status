# This guide will walk through how to create DB for AppStatus

## Pre-requisites:

SQL Server Express and SQL Server Management Studio Installed on your local computer.

## Setup:

1. Start SQL Server Management Studio.

2. Connect to your Server "(localdb)\MSSQLLocalDB".

3. Create empty DB "AppStatus". If DB already exists, drop all the tables.

4. Open and run the script "AppStatus_DB.sql" with SQL Server Management Studio.

5. Be sure connection string to the local DB is "Server=(localdb)\\MSSQLLocalDB;initial Catalog=AppStatus;Integrated Security=True".

6. Run AppStatus.