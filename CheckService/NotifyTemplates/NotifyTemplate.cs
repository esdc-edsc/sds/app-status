﻿using System.Collections.Generic;
using System.Net.Mail;

namespace GoC.AppStatus.CheckService.NotifyTemplates
{
    public abstract class NotifyTemplate : INotifyTemplate
    {
        public abstract string TemplateId { get; }

        public MailAddress Recipient { get; set; }

        public Dictionary<string, dynamic> Personalisation { get; }

        private const string PSiteUrl = "site.url";

        public NotifyTemplate() :this("////localhost:5000//") { } //indirectly referenced by ExampleTest
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1054:Uri parameters should not be strings", Justification = "Required to be string for email")]
        public NotifyTemplate(string siteUrl)
        {
            Personalisation = new Dictionary<string, dynamic>
            {
                { PSiteUrl, siteUrl }
            };
        }
    }
}
