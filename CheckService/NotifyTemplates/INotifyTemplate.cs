﻿using System.Collections.Generic;
using System.Net.Mail;

namespace GoC.AppStatus.CheckService.NotifyTemplates
{
    public interface INotifyTemplate
    {
        string TemplateId { get; }
        MailAddress Recipient { get; set; }
        Dictionary<string, dynamic> Personalisation { get; }
    }
}
