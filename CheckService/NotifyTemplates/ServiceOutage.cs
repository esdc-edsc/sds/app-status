﻿using GoC.AppStatus.DBAccess.DBEntities;
using System;
using System.Net.Mail;

namespace GoC.AppStatus.CheckService.NotifyTemplates
{
    public class ServiceOutage : NotifyTemplate, INotifyTemplate
    {
        public override string TemplateId => "e5665a0f-3afc-40c1-97bc-25a2bf5a4dca";

        //Personalization elements needed in template (defined on notify)
        public const string PServiceName = "service.name";
        public const string PServiceUrl = "service.url";
        public const string PUserName = "user.name";

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1054:Uri parameters should not be strings", Justification = "Required to be a string for email")]
        public ServiceOutage(Service service, string siteUrl)
            : base(siteUrl)
        {
            if (service == null) throw new ArgumentNullException(nameof(service));

            Personalisation.Add(PServiceName, service.Name);
            Personalisation.Add(PServiceUrl, service.Url);
        }

        public void UpdateOrAddUserDetails(User user)
        {
            if (user == null) throw new ArgumentNullException(nameof(user));

            Personalisation[PUserName] = user.ActualName;
            Recipient = new MailAddress(user.Email, user.ActualName);
        }
    }
}
