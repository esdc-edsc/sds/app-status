﻿using Hangfire;
using Hangfire.MemoryStorage;
using Microsoft.Extensions.DependencyInjection;

namespace GoC.AppStatus.CheckService.Utilities
{
    public static class HangfireManager
    {
        public static void AddHangfire(this IServiceCollection services)
        {
            services.AddHangfire(configuration => configuration
                .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                .UseSimpleAssemblyNameTypeSerializer()
                .UseRecommendedSerializerSettings()
                .UseMemoryStorage() //should change to a DB in future
            );

            services.AddHangfireServer();
        }

        public static void CreateStatusCheckJobs()
        {
            RecurringJob.AddOrUpdate<IServiceManager>(sm => sm.CheckAllServices(), "*/5 * * * *");
            //RecurringJob.AddOrUpdate<IServiceManager>(sm => sm.CheckAllServices(), "*/1 * * * *"); // PHE: toutes les minutes (pour débogage)
        }
    }
}
