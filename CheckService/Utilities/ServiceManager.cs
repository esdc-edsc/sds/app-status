﻿using GoC.AppStatus.CheckService.NotifyTemplates;
using GoC.AppStatus.DBAccess.DBEntities;
using GoC.AppStatus.DBAccess.DataAccess;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GoC.AppStatus.CheckService.Utilities
{
    public class ServiceManager : IServiceManager
    {
        private readonly ILogger<IServiceManager> _logger;
        private readonly IGCNotifyService _notify;
        private readonly IServiceTester _serviceTester;
        private readonly IServiceQueries _serviceQueries;
        private readonly ISubscriptionQueries _subscriptionQueries;
        private readonly string _siteUrl;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "Excpect valid injections")]
        public ServiceManager(ILogger<IServiceManager> logger, 
                              IGCNotifyService notifyService, 
                              IServiceTester serviceTester, 
                              IServiceQueries serviceQueries, 
                              ISubscriptionQueries subscriptionQueries,
                              IConfiguration configuration)
        {
            _logger = logger;
            _notify = notifyService;
            _serviceTester = serviceTester;
            _serviceQueries = serviceQueries;
            if(!_serviceQueries.DatabaseHasConnection())
                _serviceQueries.ConnectDatabase(configuration.GetConnectionString("DefaultConnection"));
            _subscriptionQueries = subscriptionQueries;
            if (!_subscriptionQueries.DatabaseHasConnection())
                _subscriptionQueries.ConnectDatabase(configuration.GetConnectionString("DefaultConnection"));
            _siteUrl = configuration["ApplicationInfo:SiteUrl"];
        }

        public void CheckAllServices()
        {
            var services = _serviceQueries.GetAllServices();
            int pass = 0, fail = 0;
            foreach (var service in services)
            {
                // just check subscribed services:
                if (!service.Subscribed) continue;

                var failureReport = _serviceQueries.GetActiveFailureReport(service.Id);
                var currentStatusCode = _serviceTester.GetStatusCode(new Uri(service.Url));
                if (_serviceTester.IsStatusCodeValid(currentStatusCode))
                {
                    if (failureReport != null)
                    {
                        failureReport.Resolved = DateTime.Now;
                        _serviceQueries.ReportFailedServiceResolved(failureReport);
                    }
                    pass++;
                }
                else
                {
                    fail++;
                    if (failureReport == null)
                    {
                        ReportFailedService(service);
                        failureReport = new ServiceFailureReport
                        {
                            ServiceId = service.Id,
                            FaildWith = currentStatusCode,
                            FirstReported = DateTime.Now
                        };
                        _serviceQueries.ReportFailedService(failureReport);
                    }
                }
            }
            _logger.LogInformation($"{services.Count()} Services checked: {pass} passed, {fail} failed.");
        }

        public void ReportFailedServices(List<Service> failedServices)
        {
            if (failedServices != null && failedServices.Any())
            {
                failedServices.ForEach(s => ReportFailedService(s));
            }
            else
            {
                _logger.LogInformation("No failed services to report at this time");
            }
        }

        public void ReportFailedService(Service failedService)
        {
            var subscribedUsers = GetSubscribedUsers(failedService);
            NotifySubscribersOfFailedService(subscribedUsers, failedService);
            _logger.LogInformation($"The Users ({subscribedUsers}) have been notified that {failedService} is failing.");
        }

        public IEnumerable<User> GetSubscribedUsers(Service service)
        {
            return _subscriptionQueries.GetSubscribedUsers(service);
        }

        public void NotifySubscribersOfFailedService(IEnumerable<User> subscribers, Service service)
        {
            var email = new ServiceOutage(service, _siteUrl);

            if (subscribers == null) throw new ArgumentNullException(nameof(subscribers));

            foreach (var user in subscribers)
            {
                email.UpdateOrAddUserDetails(user);
                _notify.SendEmail(email);
            }
        }
    }
}
