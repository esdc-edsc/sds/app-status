using GoC.AppStatus.CheckService.NotifyTemplates;
using Microsoft.Extensions.Configuration;
using NLog;
using Notify.Client;
using Notify.Interfaces;
using Notify.Models.Responses;
using System;

namespace GoC.AppStatus.CheckService.Utilities
{
    public class GcNotifyService : IGCNotifyService
    {
        private readonly INotificationClient _client;
        private readonly ILogger _logger;

        public GcNotifyService(IConfiguration config)
        {
            if (config == null) throw new ArgumentNullException(nameof(config));
            _client = new NotificationClient(config.GetValue<string>("NotifySettings:BaseUrl"), config.GetValue<string>("NotifySettings:ApiKey"));
            _logger = LogManager.GetCurrentClassLogger();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Should be delt with later")]
        public EmailNotificationResponse SendEmail(INotifyTemplate template)
        {
            if (template == null) throw new ArgumentNullException(nameof(template), "A template is needed in order to send an email.");

            EmailNotificationResponse response = null;

            try
            {
                response = _client.SendEmail(template.Recipient.Address, template.TemplateId, template.Personalisation);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Could not send Email");
            }

            return response;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "<Pending>")]
        public EmailNotificationResponse SendTestEmail(string emailAddress)
        {
            EmailNotificationResponse response = null;
            var template = new ExampleTest();

            try
            {
                response = _client.SendEmail(emailAddress, template.TemplateId);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Could not send Email");
            }
            return response;
        }
    }
}
