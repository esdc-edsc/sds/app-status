using GoC.AppStatus.CheckService.NotifyTemplates;
using Notify.Models.Responses;

namespace GoC.AppStatus.CheckService.Utilities
{
    public interface IGCNotifyService
    {
        EmailNotificationResponse SendEmail(INotifyTemplate notifyTemplate);
    }
}
