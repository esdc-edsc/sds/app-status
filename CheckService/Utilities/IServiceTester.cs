﻿using System;
using System.Net;

namespace GoC.AppStatus.CheckService.Utilities
{
    public interface IServiceTester
    {
        HttpStatusCode GetStatusCode(Uri uri);
        bool IsStatusCodeValid(HttpStatusCode code);
    }
}
