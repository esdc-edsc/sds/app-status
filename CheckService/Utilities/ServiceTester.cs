﻿using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Net.Http;

namespace GoC.AppStatus.CheckService.Utilities
{
    public class ServiceTester : IServiceTester
    {
        private readonly HttpClient _httpClient;
        
        private readonly ILogger<ServiceTester> _logger;

        public ServiceTester(IHttpClientFactory httpClientFactory, ILogger<ServiceTester> logger = null)
            : this(httpClientFactory.CreateClient(), logger) { }
        public ServiceTester(HttpClient httpClient, ILogger<ServiceTester> logger = null)
        {
            _httpClient = httpClient;
            if (_httpClient != null) _httpClient.DefaultRequestHeaders.Add("User-Agent", "AppStatus");
            if (logger != null) _logger = logger;
        }

        public HttpStatusCode GetStatusCode(Uri uri)
        {
            if (uri == null) return HttpStatusCode.BadRequest;
            try
            {
                return _httpClient.GetAsync(uri).GetAwaiter().GetResult().StatusCode;
            }
            catch (HttpRequestException e)
            {
                if (_logger != null) _logger.LogError(e, e.Message);
                return HttpStatusCode.NotFound;
            }
            
        }

        public bool IsStatusCodeValid(HttpStatusCode code)
        {
            return code.Equals(HttpStatusCode.OK);
        }
    }
}
