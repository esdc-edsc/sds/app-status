﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "Exception text", Scope = "member", Target = "~M:GoC.AppStatus.CheckService.Utilities.Notify.SendEmail(GoC.AppStatus.CheckService.Interfaces.INotifyTemplate)")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "Primarally internal function; no need to re-validate", Scope = "member", Target = "~M:GoC.AppStatus.CheckService.Utilities.ServiceManager.NotifySubscribersOfFailedService(System.Collections.Generic.IEnumerable{GoC.AppStatus.CheckService.Entities.User},GoC.AppStatus.CheckService.Entities.Service)")]
[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "Exception text", Scope = "member", Target = "~M:GoC.AppStatus.CheckService.Utilities.ServiceManager.ReportFailedServices(System.Collections.Generic.IEnumerable{GoC.AppStatus.CheckService.Entities.Service})")]
[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "Exception text", Scope = "member", Target = "~M:GoC.AppStatus.CheckService.Utilities.Notify.SendEmail(GoC.AppStatus.CheckService.Entities.NotifyTemplates.INotifyTemplate)~System.Boolean")]
[assembly: SuppressMessage("Globalization", "CA1303:Do not pass literals as localized parameters", Justification = "Exception text", Scope = "member", Target = "~M:GoC.AppStatus.CheckService.Utilities.GcNotifyService.SendEmail(GoC.AppStatus.CheckService.Entities.NotifyTemplates.INotifyTemplate)~Notify.Models.Responses.EmailNotificationResponse")]
