# AppStatus Change Log

## Alpha Release

### New Features

* **Monitoring** Services address for "OK" response - [_feature documenation_](https://gitlab.com/esdc-edsc/sds/app-status/-/wikis/home#monitoring)
* **Subscription** to Services will send notifactions to subscribed users - [_feature documenation_](https://gitlab.com/esdc-edsc/sds/app-status/-/wikis/home#subscription)
* **Service** Registration and Edit - [_feature documenation_](https://gitlab.com/esdc-edsc/sds/app-status/-/wikis/home#service)
* **Anonymous Authentication** - [_feature documenation_](https://gitlab.com/esdc-edsc/sds/app-status/-/wikis/home#anonymous-authentication)
