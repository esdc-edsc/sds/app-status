### Pipeline Configurations

[GitLab-Runner](https://docs.gitlab.com/runner/install/) is need to be installed and registered against the project with a `shell` executor using `powershell` and have the following executables defined in the 'PATH' (System environment variables):

* `nuget.exe`;
* `dotnet.exe`;

[Azure CLI](https://docs.microsoft.com/en-us/cli/azure/) is required for pipeline to run, since it uses Azure CLI commands to deploy the staging version and swap to production. Azure CLI can be downloaded [here](https://aka.ms/installazurecliwindows).