# Setting up your development environment

[[_TOC_]]

## Tooling

You will require access to the latest tools in order to develop on this application:

* .NET Core
* Git
* NuGet

It is suggested that you develop with Visual Studio 2019 (Community edition will work), just ensure you have the latest version installed.

## Packages

Most packages used by this project are available from nuget.org; which should be the default source for NuGet.
However GCNotify requires the _Notify_ package, only available at `https://api.bintray.com/nuget/gov-uk-notify/nuget`.
You will need to manually add this as a source to your `nuget.config`.

More detailed descriptions of some 3rd party packages can be found in the [Project Wiki](https://gitlab.com/esdc-edsc/sds/app-status/-/wikis/home).

## Database

We are using MongoDB (NoSQL). Please see the [DB Scripts README](_scripts\db\README.md) for details on setting it up.
Head to [MongoDB Docs](https://docs.mongodb.com/) for more details on how to use MongoDB.

## Secrets

**DO NOT CHECK IN YOUR SECRETS**

You will need to obtain access or generate your own secrets for the various services and connections. You can add secrets to your local enviornment by running the command `dotnet user-secrets set "CONFIG:PATH" "SECRET"`. 

- **GC Notify API Key**: `NotifySettings:ApiKey` [_more details_](https://gitlab.com/esdc-edsc/sds/app-status/-/wikis/GC-Notify-Integration)
- **Mongo DB Connection String**: `ConnectionStrings:DefaultConnection` [_more details_](_scripts/db/README.md)
