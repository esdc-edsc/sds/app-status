# Setting up Azure environment

[[_TOC_]]

## Slots

When we deploy an App on Azure, a slot is created to run the App inside it. This default slot is labelled "Production".
We created another slot, named "Staging", which has the role to receive a newer version of the AppStatus when it is deployed.
After the "Staging" new version has been approved, it just remains to click on the proper button inside Gitlab pipeline front-end to push this code to production. Please note this operation is formerly called a "swap". The code fron slot Staging is not just pushed to slot Production. In the same time, the code from Production is pushed to slot Staging.
The url of the production code is [esdcappstatus.azurewebsites.net](https://esdcappstatus.azurewebsites.net/), whereas the url of staging code is [esdcappstatus-staging.azurewebsites.net](https://esdcappstatus-staging.azurewebsites.net/).

An interesting Azure feature is the possibility to attach settings and connection strings to a slot. It's very important to note that, by default, the settings and connection strings attached to a slot are swapped during a swap operation. To avoid this, we have to check the box labelled "Deployment slot setting", which guarantees the settings attached to it won't be overriden during a swap.

## Howto attach settings and connection string to a slot

In Azure Portal, select the App/Slot (ESDCAppStatus or ESDCAppStatus/staging) and click on item Settings>Configuration inside the left menu. Click on "New application setting" to add a key and a value, or click on "New connection string" to add a connection string.

Like said previously, it's very important to check the box "Deployment slot setting" to be sure the setting/connection string will be stuck to it, and won't be overriden during a swap.

## Application settings depending on the environment

### On Azure

Azure will inject the values defined in the Configuration settings into the `appsettings.json` when deploying the application, specified by environment (defined by the slots).
This allows us to dynamically manage the configuration file from azure when needed.
It also allows us to lock environment configurations to the specified environment.

Connection Strings are handled a bit differently as they require a specific security control with .NET.
They use the _Connection Strings_ configuration section.

For configuration settings that are secrets, they should be extracted dynamically from the Azure Key Vault.
This can be done by defining the value of the configuration as `@Microsoft.KeyVault(SecretUri=https://sdskv.vault.azure.net/secrets/SecretName/xxxxxxxxx)`.
More information can be found in [Microsoft Documentation on Secrets](https://docs.microsoft.com/en-us/azure/app-service/app-service-key-vault-references).
Please review [Environment Setup Secrets](_docs/EnvironmentSetup#secrets) for configurations that are secrets.

### Locally

The settings should be defined in the source code `appsettings.json` file.
Note for settings that are secrets, each user will need to define them locally with their own values.
Please read the [Environment Setup Secrets](_docs/EnvironmentSetup#secrets) on how to do this.

### Configurations

The following are configurations that need to be defined for each environment (by slot).

#### Database Name

To identify the name of the database used in the connection.

For Production the value should be `AppStatus`.  
For Staging the value should be `AppStatusStaging`.

Defined in the `appsettings.json` as
```json
{
    "Databases": {
        "MongoDBName": "AppStatus"
    }
}
```

#### Connection String

Note that the connection string for production is the same as the one for staging (just the DB changes).

Defined as:

```json
{
    "ConnectionStrings": {
        "DefaultConnection": "mongodb://localhost:27017/AppStatus"
    }
}
```

#### Site URL

The site url is needed for when jobs are executed out of the HTTP context (the code cannot determine the url it's running at).  
Defined as: `ApplicationInfo:SiteUrl`

## Integrating NLog with Azure Application Insights

We assume here the file "nlog.config" has been updated with required rule and target `<target xsi:type="ApplicationInsightsTarget" name="whatever">`.

After to have added an Azure Application Insights resource for slot "Staging" and another one for slot "Production", we just have to verify that the `Instrumentation Key` of each Application Insights resource has been injected inside the keys `APPINSIGHTS_INSTRUMENTATIONKEY` and `APPLICATIONINSIGHTS_CONNECTION_STRING` defined for the slots (please note these keys have already defined; we don't have to define them).

Verify that box "Deployment slot setting" is checked for the previous two keys, to assure they won't be swapped during a slot swapping.

Finally, we just have to deploy and restart the slot.